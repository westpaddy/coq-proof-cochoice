Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From CO Require Import CO_Regularity.
From CO Require Import CO_RegExp.
From CO Require Import CO_Weakening.
From CO Require Import CO_TypingClosed.

Lemma wf_env_binds_inv : forall x u G,
    wf_env G ->
    binds x u G ->
    wf_typ G u \/ u = trm_star.
Proof.
  induction G using env_ind; intros.
  - false (binds_empty_inv H0).
  - binds_cases H0.
    + destruct~ (IHG (proj21 (wf_env_push_inv H)) B).
      * left. apply~ wf_typ_weaken_append.
    + subst. destruct~ (proj22 (wf_env_push_inv H)).
      * left. apply~ wf_typ_weaken_append.
Qed.

Lemma wf_env_append_inv : forall G1 G2,
    wf_env (G1 & G2) ->
    wf_env G1.
Proof.
  induction G2 using env_ind; rew_env_concat; auto.
  - intros. unpack (wf_env_push_inv H). auto.
Qed.

Lemma wf_env_wf_name_wf_effect_wf_typ_subst :
  (forall G, wf_env G -> forall G1 x u G2,
        G = G1 & x ~ u & G2 -> typ u -> wf_env (G1 & G2)) /\
  (forall G n, wf_name G n -> forall G1 x u G2,
        G = G1 & x ~ u & G2 -> typ u -> wf_name (G1 & G2) n) /\
  (forall G E, wf_effect G E -> forall G1 x u G2,
        G = G1 & x ~ u & G2 -> typ u -> wf_effect (G1 & G2) E) /\
  (forall G T, wf_typ G T -> forall G1 x u G2,
        G = G1 & x ~ u & G2 -> typ u -> wf_typ (G1 & G2) T).
Proof.
  apply wf_env_wf_name_wf_effect_wf_typ_ind; intros; subst*.
  - false* empty_middle_inv.
  - destruct G2 using env_ind; rew_env_concat in *.
    + unpack (eq_push_inv H4); subst*.
    + unpack (eq_push_inv H4); subst*.
  - destruct G2 using env_ind; rew_env_concat in *.
    + unpack (eq_push_inv H2); subst*.
    + unpack (eq_push_inv H2); subst*.
  - binds_cases H1; eauto.
    + subst. inversion H3.
  - apply_fresh wf_typ_forall.
    + apply_ih_bind* H0.
    + apply_ih_bind* H2.
Qed.

Lemma wf_env_subst : forall G' G x u,
    wf_env (G & x ~ u & G') ->
    typ u ->
    wf_env (G & G').
Proof. eauto using (proj41 wf_env_wf_name_wf_effect_wf_typ_subst). Qed.

Lemma wf_name_subst : forall G' G x u n,
    wf_name (G & x ~ u & G') n ->
    typ u ->
    wf_name (G & G') n.
Proof. eauto using (proj42 wf_env_wf_name_wf_effect_wf_typ_subst). Qed.

Lemma wf_effect_subst : forall G' G x u E,
    wf_effect (G & x ~ u & G') E ->
    typ u ->
    wf_effect (G & G') E.
Proof. eauto using (proj43 wf_env_wf_name_wf_effect_wf_typ_subst). Qed.

Lemma wf_typ_subst : forall G' G x u T,
    wf_typ (G & x ~ u & G') T ->
    typ u ->
    wf_typ (G & G') T.
Proof. eauto using (proj44 wf_env_wf_name_wf_effect_wf_typ_subst). Qed.

Local Hint Resolve wf_env_subst wf_name_subst wf_effect_subst wf_typ_subst.

Lemma typing_subst : forall G' G x e u T T' E,
    G & x ~ T' & G' |= e ~: T & E ->
    G |= u ~: T' & trm_empty ->
    G & G' |= [x ~> u]e ~: T & E.
Proof.
  intros* Typing1. gen_eq GG : (G & x ~ T' & G'). move: G G' x u T'.
  induction Typing1; simpl; intros; subst*.
  - case_var.
    + binds_get H1. apply* typing_weaken_append.
    + binds_cases H1; eauto.
  - apply_fresh* typing_abs.
    rewrite~ <- subst_open_var_trm.
    apply_ih_bind* H1.
  - rewrite* (@subst_fresh_trm x u n).
  - apply_fresh* typing_sabs.
    rewrite~ <- subst_open_nvar_trm.
    apply_ih_bind* H0.
  - apply_fresh* typing_fix.
    rewrite (_: trm_abs [x ~> u]e = [x ~> u](trm_abs e));
      first reflexivity.
    rewrite~ <- subst_open_var_trm.
    apply_ih_bind* H0.
  - rewrite (@subst_fresh_trm x u n); eauto 6.
Qed.

Lemma wf_env_wf_name_wf_effect_wf_typ_nsubst :
  (forall G, wf_env G -> forall G1 a u G2,
        G = G1 & 'a & G2 -> wf_name G1 u ->
        wf_env (G1 & map (nsubst_trm a u) G2)) /\
  (forall G n, wf_name G n -> forall G1 a u G2,
        G = G1 & 'a & G2 -> wf_name G1 u ->
        wf_name (G1 & map (nsubst_trm a u) G2) ['a ~> u]n) /\
  (forall G E, wf_effect G E -> forall G1 a u G2,
        G = G1 & 'a & G2 -> wf_name G1 u ->
        wf_effect (G1 & map (nsubst_trm a u) G2) ['a ~> u]E) /\
  (forall G T, wf_typ G T -> forall G1 a u G2,
        G = G1 & 'a & G2 -> wf_name G1 u ->
        wf_typ (G1 & map (nsubst_trm a u) G2) ['a ~> u]T).
Proof.
  apply wf_env_wf_name_wf_effect_wf_typ_ind; intros; subst;
    try solve [calc_open_subst; auto].
  - false* empty_middle_inv.
  - destruct G2 using env_ind; rew_env_concat in *.
    + unpack (eq_push_inv H4); subst.
      by autorewrite with rew_env_map rew_env_concat.
    + unpack (eq_push_inv H4); subst.
      autorewrite with rew_env_map rew_env_concat.
      by auto.
  - destruct G2 using env_ind; rew_env_concat in *.
    + unpack (eq_push_inv H2); subst.
      by autorewrite with rew_env_map rew_env_concat.
    + unpack (eq_push_inv H2); subst.
      autorewrite with rew_env_map rew_env_concat.
      by auto.
  - binds_cases H1; calc_open_subst; auto.
    + simpl_dom. false* notin_same.
    + auto using wf_name_weaken_append.
    + auto using wf_name_weaken_append.
    + apply~ wf_name_var.
      replace trm_star with ['a0 ~> u]trm_star by reflexivity.
      auto using binds_map.
  - calc_open_subst. apply_fresh wf_typ_forall.
    + rewrite~ <- nsubst_open_nvar_trm.
      replace trm_star with ['a ~> u]trm_star by reflexivity.
      apply_ih_map_bind* H0.
    + rewrite~ <- nsubst_open_nvar_trm.
      replace trm_star with ['a ~> u]trm_star by reflexivity.
      apply_ih_map_bind* H2.
Qed.

Lemma wf_env_nsubst : forall G' G a u,
    wf_env (G & 'a & G') ->
    wf_name G u ->
    wf_env (G & map (nsubst_trm a u) G').
Proof. eauto using (proj41 wf_env_wf_name_wf_effect_wf_typ_nsubst). Qed.

Lemma wf_name_nsubst : forall G' G a u n,
    wf_name (G & 'a & G') n ->
    wf_name G u ->
    wf_name (G & map (nsubst_trm a u) G') ['a ~> u]n.
Proof. eauto using (proj42 wf_env_wf_name_wf_effect_wf_typ_nsubst). Qed.

Lemma wf_effect_nsubst : forall G' G a u E,
    wf_effect (G & 'a & G') E ->
    wf_name G u ->
    wf_effect (G & map (nsubst_trm a u) G') ['a ~> u]E.
Proof. eauto using (proj43 wf_env_wf_name_wf_effect_wf_typ_nsubst). Qed.

Lemma wf_typ_nsubst : forall G' G a u T,
    wf_typ (G & 'a & G') T ->
    wf_name G u ->
    wf_typ (G & map (nsubst_trm a u) G') ['a ~> u]T.
Proof. eauto using (proj44 wf_env_wf_name_wf_effect_wf_typ_nsubst). Qed.

Local Hint Resolve wf_env_nsubst wf_name_nsubst wf_effect_nsubst wf_typ_nsubst.

Lemma typ_sub_nsubst : forall a n T1 T2,
    typ_sub T1 T2 ->
    name n ->
    typ_sub ['a ~> n]T1 ['a ~> n]T2.
Proof.
  induction 1; intro; calc_open_subst; eauto using eff_sub_nsubst.
  - apply_fresh typ_sub_forall.
    + do! rewrite~ <- nsubst_open_nvar_trm.
    + do! rewrite~ <- nsubst_open_nvar_trm.
      apply* eff_sub_nsubst.
Qed.

Lemma typing_nsubst : forall G' G a u e T E,
    G & 'a & G' |= e ~: T & E ->
    wf_name G u ->
    G & map (nsubst_trm a u) G' |= ['a ~> u]e ~: ['a ~> u]T & ['a ~> u]E.
Proof.
  intros* Typing. gen_eq GG : (G & 'a & G').
  move: G G' a u.
  induction Typing; simpl; intros; subst.
  - apply* typing_var.
    binds_cases H1.
    + apply~ binds_concat_left. rewrite~ nsubst_fresh_trm.
      rewrite <- concat_assoc in H0.
      destruct~ (wf_env_binds_inv (wf_env_append_inv H0) B0).
      * apply (wf_typ_closed H1). rew_env_concat in H0.
        eauto using ok_middle_inv_l.
      * subst. simpl. notin_solve.
    + subst. inversion H.
    + apply binds_concat_right. by apply binds_map.
  - apply typing_app with (T1 := ['a ~> u]T1);
      try solve [intros; do! rewrite~ nsubst_fresh_trm].
    + apply* IHTyping1.
    + apply* IHTyping2.
  - apply_fresh~ typing_abs.
    rewrite~ <- nsubst_open_var_trm.
    apply_ih_map_bind* H1.
  - do ! [rewrite~ nsubst_open_trm].
    apply typing_sapp with (E1 := ['a ~> u]E1);
      try solve [intros; do! rewrite~ nsubst_fresh_trm].
    + apply* IHTyping.
    + apply~ wf_name_nsubst.
    + apply (wf_effect_nsubst H0 H5).
    + rewrite~ <- nsubst_open_trm. apply (eff_sub_nsubst a u H1).
  - apply_fresh~ typing_sabs.
    do ! [rewrite~ <- nsubst_open_nvar_trm].
    replace trm_star with ['a ~> u]trm_star by reflexivity.
    apply_ih_map_bind* H0.
  - apply_fresh~ typing_fix.
    replace (trm_abs ['a ~> u]e) with ['a ~> u](trm_abs e) by reflexivity.
    rewrite~ <- nsubst_open_var_trm.
    do ! [replace (trm_arrow ['a ~> u]T1 ['a ~> u]E ['a ~>u]T2)
            with ['a ~> u](trm_arrow T1 E T2) by reflexivity].
    apply_ih_map_bind* H0.
  - apply typing_choice;
      try solve [do! rewrite~ nsubst_fresh_trm].
    + apply* IHTyping1.
    + apply* IHTyping2.
    + apply~ wf_name_nsubst.
    + apply (wf_effect_nsubst H0 H5).
    + apply (eff_sub_nsubst a u H1).
  - apply* typing_sub.
    + apply (typ_sub_nsubst a H1). auto.
    + apply (eff_sub_nsubst a u H2).
Qed.
