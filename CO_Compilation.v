Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From CO Require Import CO_Regularity.
From CO Require Import CO_Weakening.
From CO Require Import CO_Typing.

Fixpoint close_var_trm_rec (k : nat) (z : var) (t : trm) {struct t} : trm :=
  match t with
  | trm_bvar i => trm_bvar i
  | trm_empty => trm_empty
  | trm_single t1 => trm_single (close_var_trm_rec k z t1)
  | trm_dot t1 t2 => trm_dot (close_var_trm_rec k z t1) (close_var_trm_rec k z t2)
  | trm_plus t1 t2 => trm_plus (close_var_trm_rec k z t1) (close_var_trm_rec k z t2)
  | trm_kstar t1 => trm_kstar (close_var_trm_rec k z t1)
  | trm_star => trm_star
  | trm_nat => trm_nat
  | trm_arrow t1 t2 t3 => trm_arrow (close_var_trm_rec k z t1) (close_var_trm_rec k z t2) (close_var_trm_rec k z t3)
  | trm_forall t1 t2 => trm_forall (close_var_trm_rec (S k) z t1) (close_var_trm_rec (S k) z t2)
  | trm_nvar x => trm_nvar x
  | trm_eps => trm_eps
  | trm_on => trm_on
  | trm_off => trm_off
  | trm_append t1 t2 => trm_append (close_var_trm_rec k z t1) (close_var_trm_rec k z t2)
  | trm_fvar x => If x = z then (trm_bvar k) else (trm_fvar x)
  | trm_app t1 t2 => trm_app (close_var_trm_rec k z t1) (close_var_trm_rec k z t2)
  | trm_abs t1 => trm_abs (close_var_trm_rec (S k) z t1)
  | trm_sapp t1 t2 => trm_sapp (close_var_trm_rec k z t1) (close_var_trm_rec k z t2)
  | trm_sabs t1 => trm_sabs (close_var_trm_rec (S k) z t1)
  | trm_fix t1 => trm_fix (close_var_trm_rec (S k) z t1)
  | trm_choice t1 t2 t3 => trm_choice (close_var_trm_rec k z t1) (close_var_trm_rec k z t2) (close_var_trm_rec k z t3)
  end
.

Definition close_var_trm z t := close_var_trm_rec 0 z t.
Transparent close_var_trm.

Fixpoint close_nvar_trm_rec (k : nat) (z : var) (t : trm) {struct t} : trm :=
  match t with
  | trm_bvar i => trm_bvar i
  | trm_empty => trm_empty
  | trm_single t1 => trm_single (close_nvar_trm_rec k z t1)
  | trm_dot t1 t2 => trm_dot (close_nvar_trm_rec k z t1) (close_nvar_trm_rec k z t2)
  | trm_plus t1 t2 => trm_plus (close_nvar_trm_rec k z t1) (close_nvar_trm_rec k z t2)
  | trm_kstar t1 => trm_kstar (close_nvar_trm_rec k z t1)
  | trm_star => trm_star
  | trm_nat => trm_nat
  | trm_arrow t1 t2 t3 => trm_arrow (close_nvar_trm_rec k z t1) (close_nvar_trm_rec k z t2) (close_nvar_trm_rec k z t3)
  | trm_forall t1 t2 => trm_forall (close_nvar_trm_rec (S k) z t1) (close_nvar_trm_rec (S k) z t2)
  | trm_nvar x => If x = z then (trm_bvar k) else (trm_nvar x)
  | trm_eps => trm_eps
  | trm_on => trm_on
  | trm_off => trm_off
  | trm_append t1 t2 => trm_append (close_nvar_trm_rec k z t1) (close_nvar_trm_rec k z t2)
  | trm_fvar x => trm_fvar x
  | trm_app t1 t2 => trm_app (close_nvar_trm_rec k z t1) (close_nvar_trm_rec k z t2)
  | trm_abs t1 => trm_abs (close_nvar_trm_rec (S k) z t1)
  | trm_sapp t1 t2 => trm_sapp (close_nvar_trm_rec k z t1) (close_nvar_trm_rec k z t2)
  | trm_sabs t1 => trm_sabs (close_nvar_trm_rec (S k) z t1)
  | trm_fix t1 => trm_fix (close_nvar_trm_rec (S k) z t1)
  | trm_choice t1 t2 t3 => trm_choice (close_nvar_trm_rec k z t1) (close_nvar_trm_rec k z t2) (close_nvar_trm_rec k z t3)
  end
.

Definition close_nvar_trm z t := close_nvar_trm_rec 0 z t.
Transparent close_nvar_trm.

Fixpoint transl_typ (t : styp) {struct t} : trm :=
  match t with
  | styp_nat => trm_nat
  | styp_arrow t1 t2 =>
    trm_arrow (transl_typ t1)
              trm_empty
              (trm_forall (trm_dot (trm_single (trm_bvar 0))
                                   (trm_kstar (trm_plus (trm_single trm_on)
                                                        (trm_single trm_off))))
                          (transl_typ t2))
  end
.

Require Import Omega.
Require Import Recdef.

Fixpoint size_strm (t : strm) {struct t} : nat :=
  match t with
  | strm_bvar i => 1
  | strm_fvar x => 1
  | strm_app t1 t2 => 1 + (size_strm t1) + (size_strm t2)
  | strm_abs t1 => 1 + (size_strm t1)
  | strm_fix t1 => 1 + (size_strm t1)
  | strm_choice t1 t2 => 1 + (size_strm t1) + (size_strm t2)
  end
.

Lemma open_var_size_eq : forall x t,
    size_strm (t ^ x) = size_strm t.
Proof.
  intros x t. unfold open_strm. generalize 0.
  induction t; intro; calc_open_subst.
  - omega.
  - omega.
  - omega.
  - rewrite IHt1. rewrite IHt2. omega.
  - rewrite IHt. omega.
  - rewrite IHt. omega.
  - rewrite IHt1. rewrite IHt2. omega.
Qed.

Notation "[[ x ~> y ]] t" := ([x ~> strm_fvar y]t)%s (at level 0, t at level 0) : s_scope.

Lemma rename_size_eq : forall x y t,
    size_strm t = size_strm [[x ~> y]]t.
Proof. induction t; calc_open_subst; auto. Qed.

Lemma strm_size_ind : forall P : strm -> Prop,
    (forall x, P (strm_fvar x)) ->
    (forall t1 t2,
        sexpr t1 -> P t1 -> sexpr t2 -> P t2 -> P (strm_app t1 t2)) ->
    (forall L t1,
        (forall x, x \notin L -> sexpr (t1 ^ x)) ->
        (forall t2 x, x \notin fv_strm t2 -> size_strm t2 = size_strm t1 ->
                 sexpr (t2 ^ x) -> P (t2 ^ x)%s) ->
        P (strm_abs t1)) ->
    (forall L t1,
        (forall x, x \notin L -> sexpr (t1 ^ x)) ->
        (forall t2 x, x \notin fv_strm t2 -> size_strm t2 = size_strm t1 ->
                 sexpr (t2 ^ x) -> P (t2 ^ x)%s) ->
        P (strm_fix t1)) ->
    (forall t1 t2,
        sexpr t1 -> P t1 -> sexpr t2 -> P t2 -> P (strm_choice t1 t2)) ->
    (forall t, sexpr t -> P t).
Proof.
  intros P Ha Hb Hc Hd He t. gen_eq n: (size_strm t). gen t.
  induction n using (well_founded_induction lt_wf).
  intros* Eq Expr. subst. inverts Expr; simpl in H.
  - apply Ha.
  - apply~ Hb.
    + apply~ (H (size_strm e1)). omega.
    + apply~ (H (size_strm e2)). omega.
  - apply* Hc. intros. apply~ (H (size_strm e)). rewrite <- H2.
    rewrite~ open_var_size_eq.
  - apply* Hd. intros. apply~ (H (size_strm e)). rewrite <- H2.
    rewrite~ open_var_size_eq.
  - apply~ He.
    + apply~ (H (size_strm e1)). omega.
    + apply~ (H (size_strm e2)). omega.
Qed.

Function transl (t : strm) (a : var) (n : trm) {measure size_strm t} : trm :=
  match t with
  | strm_bvar i => trm_star
  | strm_fvar x => trm_fvar x
  | strm_app t1 t2 =>
    trm_sapp (trm_app (transl t1 a (trm_append (trm_append n trm_on) trm_on))
                      (transl t2 a (trm_append (trm_append n trm_on) trm_off)))
             (trm_append (trm_append (trm_nvar a) n) trm_off)
  | strm_abs t1 =>
    let x := var_gen (fv_strm t1) in
    trm_abs (close_var_trm x (trm_sabs (close_nvar_trm a (transl (t1 ^ x) a n))))
  | strm_fix t1 =>
    let f := var_gen (fv_strm t1) in
    trm_fix (close_var_trm f (transl (t1 ^ f) a n))
  | strm_choice t1 t2 =>
    trm_choice (transl t1 a (trm_append n trm_on))
               (trm_append (trm_append (trm_nvar a) n) trm_off)
               (transl t2 a (trm_append n trm_on))
  end.
Proof.
  all: intros; subst; try rewrite open_var_size_eq; simpl; omega.
Defined.

Lemma transl_typ_regular : forall T,
    typ (transl_typ T).
Proof.
  induction T; simpl; auto. apply~ typ_arrow. apply_fresh typ_forall.
  - calc_open_subst; auto.
  - calc_open_subst; rewrite- open_typ; auto.
Qed.

Lemma close_var_fresh : forall x t i,
    x \notin fv_trm t ->
    close_var_trm_rec i x t = t.
Proof. induction t; simpl; intros i Fr; fequals~. case_var~. Qed.

Lemma notin_close_var_trm : forall x t,
    x \notin fv_trm (close_var_trm x t).
Proof.
  intros. unfold close_var_trm. generalize 0.
  induction t; calc_open_subst; auto.
Qed.

Lemma notin_close_var_trm_keep_rec : forall x y t i,
    (x <> y -> x \notin fv_trm t) ->
    x \notin fv_trm (close_var_trm_rec i y t).
Proof.
  induction t; calc_open_subst; intros;
    do? (match goal with
         | [ H: ?x <> ?y -> ?x \notin ?V1 \u ?V2 |- _ ] =>
           assert (x <> y -> x \notin V1);
           [> intro; forwards~ :H | assert (x <> y -> x \notin V2);
                                    [> intro; forwards~ :H | clear H] ]
         end); auto.
  apply notin_singleton_l. intro. subst. apply* notin_same.
Qed.

Lemma notin_close_nvar_trm_keep_rec : forall x a t i,
    x \notin fv_trm t ->
    x \notin fv_trm (close_nvar_trm_rec i a t).
Proof. induction t; calc_open_subst; auto. Qed.

Lemma close_var_open_var_ind : forall x y z t1 i j,
  i <> j -> y <> x -> y \notin (fv_trm t1) ->
    {i ~> trm_fvar y} ({j ~> trm_fvar z} (close_var_trm_rec j x t1) )
  = {j ~> trm_fvar z} (close_var_trm_rec j x ({i ~> trm_fvar y}t1) ).
Proof. induction t1; simpl; intros; calc_open_subst; f_equal; auto. Qed.

Lemma close_var_open_nvar_ind : forall x a z t1 i j,
  i <> j -> a \notin (fnv_trm t1) ->
    {i ~> trm_nvar a} ({j ~> trm_fvar z} (close_var_trm_rec j x t1) )
  = {j ~> trm_fvar z} (close_var_trm_rec j x ({i ~> trm_nvar a}t1) ).
Proof. induction t1; simpl; intros; calc_open_subst; f_equal; auto. Qed.

Definition body t :=
  exists L, forall x, x \notin L -> expr (t ^ x).

Lemma close_var_trm_body : forall x e,
    expr e ->
    body (close_var_trm x e).
Proof.
  intros* Exp. exists \{x}. intros y Fry.
  unfold open_trm, close_var_trm. generalize 0.
  induction Exp; intros; calc_open_subst; auto.
  - apply_fresh expr_abs. unfold open_trm. rewrite* close_var_open_var_ind.
  - apply~ expr_sapp. rewrite~ close_var_fresh. rewrite~ <- open_name.
  - apply_fresh expr_sabs. unfold open_trm. rewrite* close_var_open_nvar_ind.
  - apply_fresh expr_fix. unfold open_trm. rewrite* close_var_open_var_ind.
  - apply~ expr_choice. rewrite~ close_var_fresh. rewrite~ <- open_name.
Qed.

Lemma close_nvar_open_var_ind : forall x y z t1 i j,
  i <> j -> y \notin (fv_trm t1) ->
    {i ~> trm_fvar y} ({j ~> trm_nvar z} (close_nvar_trm_rec j x t1) )
  = {j ~> trm_nvar z} (close_nvar_trm_rec j x ({i ~> trm_fvar y}t1) ).
Proof. induction t1; simpl; intros; calc_open_subst; f_equal; auto. Qed.

Lemma close_nvar_open_nvar_ind : forall x y z t1 i j,
  i <> j -> y <> x -> y \notin (fnv_trm t1) ->
    {i ~> trm_nvar y} ({j ~> trm_nvar z} (close_nvar_trm_rec j x t1) )
  = {j ~> trm_nvar z} (close_nvar_trm_rec j x ({i ~> trm_nvar y}t1) ).
Proof. induction t1; simpl; intros; calc_open_subst; f_equal; auto. Qed.

Definition nbody t :=
  exists L, forall a, a \notin L -> expr (t ^ 'a).

Lemma close_nvar_trm_nbody : forall a e,
    expr e ->
    nbody (close_nvar_trm a e).
Proof.
  intros* Exp. exists \{a}. intros y Fry.
  unfold open_trm, close_nvar_trm. generalize 0.
  induction Exp; intros; calc_open_subst; auto.
  - apply_fresh expr_abs. unfold open_trm. rewrite* close_nvar_open_var_ind.
  - apply~ expr_sapp. induction H; calc_open_subst; auto.
  - apply_fresh expr_sabs. unfold open_trm. rewrite* close_nvar_open_nvar_ind.
  - apply_fresh expr_fix. unfold open_trm. rewrite* close_nvar_open_var_ind.
  - apply~ expr_choice. induction H; calc_open_subst; auto.
Qed.

Lemma expr_abs_from_body : forall t,
    body t ->
    expr (trm_abs t).
Proof. intros. destruct H. apply_fresh* expr_abs. Qed.

Lemma expr_sabs_from_nbody : forall t,
    nbody t ->
    expr (trm_sabs t).
Proof. intros. destruct H. apply_fresh* expr_sabs. Qed.

Lemma expr_fix_from_body : forall t,
    body t ->
    expr (trm_fix t).
Proof. intros. destruct H. apply_fresh* expr_fix. Qed.

Lemma transl_regular : forall e a n,
    sexpr e ->
    name n ->
    expr (transl e a n).
Proof.
  intros*.
  functional induction (transl e a n); intros Exp Name; inverts Exp; auto 6.
  - apply expr_abs_from_body.
    apply close_var_trm_body.
    apply expr_sabs_from_nbody.
    apply close_nvar_trm_nbody.
    apply~ IHt.
    apply~ open_abs_body_sexpr.
    apply_fresh* sexpr_abs.
  - apply expr_fix_from_body.
    apply close_var_trm_body.
    apply~ IHt.
    apply~ open_fix_body_sexpr.
    apply_fresh* sexpr_fix.
Qed.

Hint Resolve transl_typ_regular transl_regular.

Lemma transl_typ_sound_base : forall T,
    wf_typ empty (transl_typ T).
Proof.
  induction T; simpl; auto.
  - apply~ wf_typ_arrow. apply_fresh wf_typ_forall.
    + calc_open_subst. auto 7.
    + unfold open_trm. rewrite~ <- open_typ.
      apply_empty~ wf_typ_weaken.
Qed.

Lemma transl_typ_sound : forall G T,
    wf_env G ->
    wf_typ G (transl_typ T).
Proof.
  intros.
  rewrite <- (concat_empty_l G).
  apply wf_typ_weaken_append.
  - apply transl_typ_sound_base.
  - by rew_env_concat.
Qed.

Lemma transl_env_sound : forall G,
    ok G ->
    wf_env (map transl_typ G).
Proof.
  induction 1.
  - rewrite~ map_empty.
  - rewrite~ map_push. apply~ wf_env_push. apply~ transl_typ_sound.
Qed.

Lemma close_var_fresh_trm_rec : forall x t i,
    x \notin fv_trm t ->
    close_var_trm_rec i x t = t.
Proof.
  induction t; simpl; intros; f_equal; calc_open_subst; auto.
Qed.

Lemma close_var_subst_trm_rec : forall x t z u i,
    x \notin fv_trm u ->
    x <> z ->
    close_var_trm_rec i x ([z~>u]t) = [z~>u](close_var_trm_rec i x t).
Proof.
  induction t; intros* Fr Neq; calc_open_subst; fequals~.
  apply~ close_var_fresh_trm_rec.
Qed.

Lemma close_nvar_fresh_trm_rec : forall a t i,
    a \notin fnv_trm t ->
    close_nvar_trm_rec i a t = t.
Proof.
  induction t; simpl; intros; f_equal; calc_open_subst; auto.
Qed.

Lemma close_nvar_subst_trm_rec : forall a x u t i,
    a \notin fnv_trm u ->
    [x ~> u](close_nvar_trm_rec i a t) = close_nvar_trm_rec i a [x ~> u]t.
Proof.
  induction t; simpl; intros; f_equal; calc_open_subst; auto.
  - rewrite~ close_nvar_fresh_trm_rec.
Qed.

Lemma close_var_rename_trm_rec : forall y x t i,
  y \notin fv_trm t ->
  close_var_trm_rec i y ([x ~> trm_fvar y]t) = close_var_trm_rec i x t.
Proof.
  induction t; simpl; intros; f_equal; auto.
  - by calc_open_subst.
Qed.

Notation "[[ x ~> y ]] t" := ([x ~> trm_fvar y]t) (at level 0, t at level 0).

Lemma close_var_rename_trm : forall y x t,
    y \notin fv_trm t ->
    close_var_trm x t = close_var_trm y [[x ~> y]]t.
Proof. intros. unfold close_var_trm. by rewrite close_var_rename_trm_rec. Qed.

Notation "[[ ' a ~> ' b ]] t" := (['a ~> trm_nvar b]t) (at level 0, t at level 0).

Lemma close_nvar_rename_trm_rec : forall b a t i,
    b \notin fnv_trm t ->
    close_nvar_trm_rec i a t = close_nvar_trm_rec i b [['a ~> 'b]]t.
Proof. induction t; simpl; intros; calc_open_subst; f_equal; auto. Qed.

Lemma close_nvar_rename_trm : forall b a t,
    b \notin fnv_trm t ->
    close_nvar_trm a t = close_nvar_trm b [['a ~> 'b]]t.
Proof. intros. unfold close_nvar_trm. by apply close_nvar_rename_trm_rec. Qed.

Lemma open_var_trm_inj : forall x t1 t2,
    x \notin (fv_trm t1) ->
    x \notin (fv_trm t2) ->
    t1 ^ x = t2 ^ x ->
    t1 = t2.
Proof.
  intros x t1. unfold open_trm. generalize 0.
  induction t1; destruct t2; simpl; intros* Fr1 Fr2; calc_open_subst;
    intros H; inverts H; try solve [notin_false | f_equal; eauto].
Qed.

Lemma open_nvar_trm_inj : forall a t1 t2,
    a \notin (fnv_trm t1) ->
    a \notin (fnv_trm t2) ->
    t1 ^ 'a = t2 ^ 'a ->
    t1 = t2.
Proof.
  intros x t1. unfold open_trm. generalize 0.
  induction t1; destruct t2; simpl; intros* Fr1 Fr2; calc_open_subst;
    intros H; inverts H; try solve [notin_false | f_equal; eauto].
Qed.

Lemma close_var_open_var_trm_ind : forall x y z t i j,
    i <> j ->
    y <> x ->
    y \notin (fv_trm t) ->
    {i ~> trm_fvar y}({j ~> trm_fvar z}(close_var_trm_rec j x t)) =
    {j ~> trm_fvar z}(close_var_trm_rec j x ({i ~> trm_fvar y}t)).
Proof. induction t; simpl; intros; calc_open_subst; f_equal; auto. Qed.

Lemma close_var_open_nvar_trm_ind : forall x b z t i j,
    i <> j ->
    {i ~> trm_nvar b}({j ~> trm_fvar z}(close_var_trm_rec j x t)) =
    {j ~> trm_fvar z}(close_var_trm_rec j x {i ~> trm_nvar b}t).
Proof. induction t; simpl; intros; calc_open_subst; f_equal; auto. Qed.

Lemma close_nvar_open_var_trm_ind : forall a y c t i j,
    i <> j ->
    {i ~> trm_fvar y}({j ~> trm_nvar c}(close_nvar_trm_rec j a t)) =
    {j ~> trm_nvar c}(close_nvar_trm_rec j a ({i ~> trm_fvar y}t)).
Proof. induction t; simpl; intros; calc_open_subst; f_equal; auto. Qed.

Lemma close_nvar_open_nvar_trm_ind : forall a b c t i j,
    i <> j ->
    b <> a ->
    b \notin (fnv_trm t) ->
    {i ~> trm_nvar b}({j ~> trm_nvar c}(close_nvar_trm_rec j a t)) =
    {j ~> trm_nvar c}(close_nvar_trm_rec j a ({i ~> trm_nvar b}t)).
Proof. induction t; simpl; intros; calc_open_subst; f_equal; auto. Qed.

Lemma close_var_name_open_rec  : forall x n i,
    name n ->
    n = {i ~> trm_fvar x}(close_var_trm_rec i x n).
Proof. induction 1; intros; calc_open_subst; f_equal; auto. Qed.

Lemma close_var_expr_open : forall x e,
    expr e ->
    e = (close_var_trm x e) ^ x.
Proof.
  intros* Expr. unfold close_var_trm, open_trm. generalize 0.
  induction Expr; intros; calc_open_subst; f_equal; auto.
  - match goal with |- _ = ?t => pick_fresh y from (fv_trm t) end.
    apply~ (@open_var_trm_inj y).
    unfold open_trm in *. rewrite~ close_var_open_var_trm_ind.
  - apply~ close_var_name_open_rec.
  - match goal with |- _ = ?t => pick_fresh a from (fnv_trm t) end.
    apply~ (@open_nvar_trm_inj a).
    unfold open_trm in *. rewrite~ close_var_open_nvar_trm_ind.
  - match goal with |- _ = ?t => pick_fresh y from (fv_trm t) end.
    apply~ (@open_var_trm_inj y).
    unfold open_trm in *. rewrite~ close_var_open_var_trm_ind.
  - apply~ close_var_name_open_rec.
Qed.

Lemma close_nvar_name_open_rec : forall a n i,
    name n ->
    n = {i ~> trm_nvar a}(close_nvar_trm_rec i a n).
Proof. induction 1; intros; calc_open_subst; f_equal; auto. Qed.

Lemma close_nvar_expr_open : forall a e,
    expr e ->
    e = (close_nvar_trm a e) ^ 'a.
Proof.
  intros* Expr. unfold close_nvar_trm, open_trm. generalize 0.
  induction Expr; intros; calc_open_subst; f_equal; auto.
  - match goal with |- _ = ?t => pick_fresh y from (fv_trm t) end.
    apply~ (@open_var_trm_inj y).
    unfold open_trm in *. rewrite~ close_nvar_open_var_trm_ind.
  - apply~ close_nvar_name_open_rec.
  - match goal with |- _ = ?t => pick_fresh b from (fnv_trm t) end.
    apply~ (@open_nvar_trm_inj b).
    unfold open_trm in *. rewrite~ close_nvar_open_nvar_trm_ind.
  - match goal with |- _ = ?t => pick_fresh y from (fv_trm t) end.
    apply~ (@open_var_trm_inj y).
    unfold open_trm in *. rewrite~ close_nvar_open_var_trm_ind.
  - apply~ close_nvar_name_open_rec.
Qed.

Lemma notin_fv_open_strm : forall a u t,
    a \notin fv_strm u ->
    a \notin fv_strm t ->
    a \notin fv_strm (t ^^ u).
Proof.
  intros*. calc_open_subst. generalize 0.
  induction t; simpl; intros; calc_open_subst; auto.
Qed.

Lemma notin_fv_transl : forall x e a n,
    sexpr e ->
    name n ->
    x \notin fv_strm e ->
    x \notin fv_trm (transl e a n).
Proof.
  intros*.
  functional induction (transl e a n); intros* Expr Name Fr; inverts Expr; try solve [simpls~].
  - apply notin_close_var_trm_keep_rec. intro.
    apply notin_close_nvar_trm_keep_rec.
    apply~ IHt.
    + pick_fresh y. rewrite~ (@subst_intro_strm y).
    + apply~ notin_fv_open_strm. simpls~.
  - apply notin_close_var_trm_keep_rec. intro.
    apply~ IHt.
    + pick_fresh y. rewrite~ (@subst_intro_strm y).
    + apply~ notin_fv_open_strm. simpls~.
Qed.

Lemma close_nvar_trm_rename : forall x y a t,
    [[x ~> y]](close_nvar_trm a t) = close_nvar_trm a [[x ~> y]]t.
Proof.
  intros*. unfold close_nvar_trm. generalize 0.
  induction t; calc_open_subst; intro; f_equal; auto.
Qed.

Lemma transl_rename : forall x y e a n,
    sexpr e ->
    name n ->
    y \notin fv_strm e ->
    [[x ~> y]](transl e a n) = transl [[x ~> y]]e a n.
Proof.
  intros* Expr. move: x y a n.
  induction Expr using strm_size_ind; intros* Name Fr; simpl in Fr;
    rewrite[in X in X = _] transl_equation;
    rewrite[in X in _ = X] transl_equation;
    calc_open_subst; do! f_equal; auto.
  - apply~ subst_fresh_trm.
  - unfold close_var_trm. simpl. f_equal.
    set z := var_gen (fv_strm [[x ~> y]]t1).
    set z' := var_gen (fv_strm t1).
    set ta := ([[x ~> y]]t1)%s.
    replace ({0 ~> strm_fvar z}ta)%s with (ta ^ z)%s by reflexivity.
    replace ({0 ~> strm_fvar z'}t1)%s with (t1 ^ z')%s by reflexivity.
    pick_fresh zz. rewrite[in X in _ = X] (@subst_intro_strm zz); auto.
    rewrite~ <- H0.
    + rewrite <- close_nvar_trm_rename.
      rewrite~ (@subst_intro_strm zz). subst ta.
      rewrite~ <- subst_open_var_strm.
      assert (y \notin fv_strm (t1 ^ zz)).
      { apply~ notin_fv_open_strm. simpls~. }
      assert (z' \notin fv_strm (t1 ^ zz)).
      { apply~ notin_fv_open_strm. simpls~. subst z'. apply var_gen_spec. }
      rewrite~ close_var_rename_trm_rec.
      rewrite~ <- H0. rewrite~ <- H0.
      rewrite <- close_nvar_trm_rename.
      rewrite~ close_var_rename_trm_rec.
      rewrite <- close_nvar_trm_rename.
      rewrite~ close_var_subst_trm_rec.
      * simpls~.
      * apply notin_close_nvar_trm_keep_rec. apply~ notin_fv_transl.
      * apply notin_close_nvar_trm_keep_rec. rewrite~ subst_open_var_strm.
        apply~ notin_fv_transl.
        -- rewrite~ <- subst_open_var_strm.
        -- apply~ notin_fv_open_strm. simpls~. subst z. apply var_gen_spec.
    + subst ta. rewrite~ <- rename_size_eq.
    + subst ta. rewrite~ <- subst_open_var_strm.
    + subst ta. subst z. apply~ notin_fv_open_strm; simpls~. apply var_gen_spec.
  - set z := var_gen (fv_strm [[x ~> y]]t1).
    set z' := var_gen (fv_strm t1).
    set ta := ([[x ~> y]]t1)%s.
    replace ({0 ~> strm_fvar z}ta)%s with (ta ^ z)%s by reflexivity.
    replace ({0 ~> strm_fvar z'}t1)%s with (t1 ^ z')%s by reflexivity.
    pick_fresh zz. rewrite[in X in _ = X] (@subst_intro_strm zz); auto.
    rewrite~ <- H0.
    + rewrite~ <- close_var_rename_trm.
      * rewrite~ (@subst_intro_strm zz). subst ta.
        rewrite~ <- subst_open_var_strm.
        assert (y \notin fv_strm (t1 ^ zz)).
        { apply~ notin_fv_open_strm. simpls~. }
        assert (z' \notin fv_strm (t1 ^ zz)).
        { apply~ notin_fv_open_strm. simpls~. subst z'. apply var_gen_spec. }
        rewrite~ <- H0. rewrite~ <- H0.
        rewrite~ <- close_var_rename_trm.
        symmetry. apply~ close_var_subst_trm_rec.
        -- simpls~.
        -- apply~ notin_fv_transl.
      * subst ta. apply~ notin_fv_transl.
        -- rewrite~ <- subst_open_var_strm.
        -- subst z. apply~ notin_fv_open_strm; simpls~. apply var_gen_spec.
    + subst ta. rewrite~ <- rename_size_eq.
    + subst ta. rewrite~ <- subst_open_var_strm.
    + subst ta. subst z. apply~ notin_fv_open_strm; simpls~. apply var_gen_spec.
  - apply~ subst_fresh_trm.
Qed.

Definition closed_trm t :=
  forall x, x \notin (fv_trm t \u fnv_trm t).

Hint Extern 1 (?x \notin fv_trm ?t) =>
match goal with
| [ H: closed_trm t |- _ ] => solve [specialize (H x); notin_solve]
end.

Hint Extern 1 (?x \notin fnv_trm ?t) =>
match goal with
| [ H: closed_trm t |- _ ] => solve [specialize (H x); notin_solve]
end.

Lemma closed_trm_eps :
  closed_trm trm_eps.
Proof. unfold closed_trm. simpl. auto. Qed.

Lemma closed_trm_on :
  closed_trm trm_on.
Proof. unfold closed_trm. simpl. auto. Qed.

Lemma closed_trm_off :
  closed_trm trm_off.
Proof. unfold closed_trm. simpl. auto. Qed.

Lemma closed_trm_append : forall n1 n2,
    closed_trm n1 ->
    closed_trm n2 ->
    closed_trm (trm_append n1 n2).
Proof.
  unfold closed_trm. intros. simpl.
  specialize (H x). specialize (H0 x).
  auto.
Qed.

Lemma closed_trm_append_inv : forall n1 n2,
    closed_trm (trm_append n1 n2) ->
    closed_trm n1 /\ closed_trm n2.
Proof.
  unfold closed_trm. simpl. intros. split; intro x; specialize(H x); auto.
Qed.

Hint Resolve closed_trm_eps closed_trm_on closed_trm_off closed_trm_append : closed_name.

Hint Extern 1 (closed_trm ?n) =>
match goal with
| [ H: closed_trm (trm_append n _) |- _ ] => apply (proj21 (closed_trm_append_inv H))
| [ H: closed_trm (trm_append _ n) |- _ ] => apply (proj22 (closed_trm_append_inv H))
end : closed_name.

Lemma closed_name_wf : forall G n,
    name n ->
    closed_trm n ->
    wf_env G ->
    wf_name G n.
Proof.
  induction 1; intros Closed WfEnv; auto.
  - specialize (Closed a). false notin_same. apply* notin_union_r2.
  - auto with closed_name.
Qed.

Lemma close_var_nvar_comm_trm : forall x a t i j,
    i <> j ->
    close_var_trm_rec i x (close_nvar_trm_rec j a t) =
    close_nvar_trm_rec j a (close_var_trm_rec i x t).
Proof. induction t; intros; calc_open_subst; f_equal; auto. Qed.

Lemma notin_fnv_close_nvar_trm_rec : forall a t i,
    a \notin fnv_trm (close_nvar_trm_rec i a t).
Proof.
  induction t; calc_open_subst; auto.
Qed.

Lemma transl_rename_sbody : forall a b e n,
    sexpr e ->
    name n ->
    closed_trm n ->
    close_nvar_trm a (transl e a n) = close_nvar_trm b (transl e b n).
Proof.
  intros* Expr Name Closed. unfold close_nvar_trm. generalize 0.
  functional induction (transl e a n); intro; inverts Expr;
    rewrite[in X in _ = X] transl_equation; calc_open_subst; try congruence.
  - f_equal.
    + f_equal; auto with closed_name.
    + do! rewrite~ close_nvar_fresh_trm_rec.
  - f_equal. f_equal. do 2 rewrite~ <- close_var_nvar_comm_trm. f_equal.
    rewrite~ close_nvar_fresh_trm_rec.
    rewrite~ close_nvar_fresh_trm_rec.
    apply~ IHt.
    pick_fresh y. rewrite~ (@subst_intro_strm y).
    apply notin_fnv_close_nvar_trm_rec.
    apply notin_fnv_close_nvar_trm_rec.
  - f_equal. unfold close_var_trm. do 2 rewrite~ <- close_var_nvar_comm_trm.
    f_equal. apply~ IHt. pick_fresh y. rewrite~ (@subst_intro_strm y).
  - f_equal; auto with closed_name.
    do! rewrite~ close_nvar_fresh_trm_rec.
Qed.

Lemma transl_sound : forall G e T a n,
    (G |= e ~: T)%s ->
    a # G ->
    name n ->
    closed_trm n ->
    map transl_typ G & 'a |= transl e a n ~: transl_typ T &
    (trm_dot (trm_single (trm_append (trm_nvar a) n))
             (trm_kstar (trm_plus (trm_single trm_on)
                                  (trm_single trm_off)))).
Proof.
  intros* Typing. move: a n.
  induction Typing; intros* Ndom Name Closed; rewrite transl_equation.
  - assert (K1: wf_env (map transl_typ G & 'a)).
    { apply~ wf_env_spush. apply~ transl_env_sound. }
    assert (K2: wf_name (map transl_typ G & 'a) n).
    { apply~ closed_name_wf. }
    apply typing_sub_E with (E1 := trm_empty); auto 6.
    + apply* typing_var. apply~ binds_concat_left_ok.
    + admit.                    (* RE *)
  - assert (K1: wf_env (map transl_typ G & 'a)).
    { apply~ wf_env_spush. apply~ transl_env_sound. }
    assert (K2: wf_typ (map transl_typ G & 'a) (transl_typ T2)).
    { apply~ transl_typ_sound. }
    assert (K3: wf_name (map transl_typ G & 'a) n).
    { apply~ closed_name_wf. }
    forwards~ K4: (IHTyping1 a (trm_append (trm_append n trm_on) trm_on));
      first by auto with closed_name.
    forwards~ K5: (IHTyping2 a (trm_append (trm_append n trm_on) trm_off));
      first by auto with closed_name.
    set E0 := trm_kstar (trm_plus (trm_single trm_on) (trm_single trm_off)).
    set E1 := trm_dot (trm_single trm_on) (trm_dot (trm_single trm_on) E0).
    set E2 := trm_dot (trm_single trm_on) (trm_dot (trm_single trm_off) E0).
    set E3 := trm_dot (trm_single trm_off) E0.
    apply typing_sub_E with
        (E1 := trm_dot (trm_single (trm_append (trm_nvar a) n))
                       (trm_plus (trm_plus trm_empty (trm_plus E1 E2)) E3)).
    + replace (transl_typ T2) with
          ((transl_typ T2) ^^ (trm_append (trm_append (trm_nvar a) n) trm_off));
        last by (unfold open_trm; rewrite~ <- open_typ).
      apply typing_sapp with (E1 := trm_dot (trm_single (trm_bvar 0)) E0).
      * apply typing_app with (T1 := transl_typ T1).
        -- apply (typing_sub K4).
           ++ apply~ wf_typ_arrow.
              ** set TG := map transl_typ G & 'a.
                 apply_fresh wf_typ_forall.
                 --- calc_open_subst. subst TG.
                     assert (wf_env ((map transl_typ G & 'a) & 'y)).
                     { apply~ wf_env_spush. }
                     auto 6.
                 --- unfold open_trm. rewrite~ <- open_typ.
                     apply transl_typ_sound.
                     subst TG. apply~ wf_env_spush.
           ++ subst E1. subst E0. auto 8.
           ++ subst E0.
              apply typ_sub_arrow; auto using typ_sub_refl with eff_sub.
              admit.           (* RE *)
           ++ subst E1. subst E0. admit. (* RE *)
        -- apply (typing_sub_E K5).
           ++ subst E2. subst E0. auto 10.
           ++ subst E2. subst E0. admit. (* RE *)
        -- admit.                        (* empty /\ E = empty *)
        -- admit.                        (* disjoint *)
        -- admit.                        (* disjiont *)
        -- simpl. auto.
        -- subst E1. subst E0. simpl. auto.
        -- subst E2. subst E0. simpl. auto.
      * auto.
      * subst E3. subst E0. auto 10.
      * subst E3. subst E0. calc_open_subst. admit. (* RE *)
      * admit.                                      (* disjoint *)
      * subst E1. subst E2. subst E0. simpl. auto.
      * subst E3. subst E0. simpl. auto.
    + subst E0. auto 10.
    + admit.                    (* RE *)
  - assert (K1: wf_env (map transl_typ G & 'a)).
    { apply~ wf_env_spush. apply~ transl_env_sound.
      specialize_fresh. eapply ok_concat_inv_l.
      apply* styping_regular_G. }
    (* assert (K2: wf_typ (map transl_typ G & 'a) (transl_typ T)). *)
    (* { apply~ transl_typ_sound. } *)
    assert (K3: wf_name (map transl_typ G & 'a) n).
    { apply~ closed_name_wf. }
    apply typing_sub_E with (E1 := trm_empty).
    + apply~ typing_weaken_append.
      set x' := var_gen (fv_strm e).
      assert (K4: sexpr (e ^ x')).
      { pick_fresh y. forwards~ : (H y).
        rewrite~ (@subst_intro_strm y). }
      simpl.
      set e' := trm_sabs (close_nvar_trm a (transl (e ^ x') a n)).
      apply_fresh~ typing_abs. subst e'.
      rewrite~ (@close_var_rename_trm y); last by (simpl; notin_solve).
      rewrite~ <- close_var_expr_open.
      * simpl.
        set e' := transl (e ^ x') a n.
        apply_fresh~ typing_sabs. subst e'.
        rewrite~ (@transl_rename_sbody a y0).
        assert (Eq: trm_nvar y0 = [[x' ~> y]](trm_nvar y0)) by reflexivity.
        rewrite Eq.
        rewrite~ <- subst_open_trm.
        rewrite- Eq.
        rewrite~ <- close_nvar_expr_open.
        rewrite~ transl_rename.
        -- rewrite~ subst_open_strm.
           calc_open_subst.
           rewrite~ subst_fresh_strm; last by apply var_gen_spec.
           rewrite~ <- (@open_typ 0 (trm_nvar y0) (transl_typ T2)).
           rewrite- map_push.
           eapply typing_sub.
           ++ apply~ H0.
           ++ apply~ transl_typ_sound.
              forwards~ : (H y).
              auto using transl_env_sound.
           ++ admit.
           ++ apply~ typ_sub_refl.
           ++ admit.            (* RE *)
        -- apply~ notin_fv_open_strm. simpls~.
      * apply~ subst_expr.
        apply expr_sabs_from_nbody.
        apply close_nvar_trm_nbody.
        apply~ transl_regular.
    + auto 10.
    + admit.                    (* RE *)
  - set x' := var_gen (fv_strm (strm_abs e)).
    simpl.
    apply typing_sub_E with (E1 := trm_empty).
    + unfold close_var_trm, open_strm.
      set e' := close_var_trm_rec 0 x' (transl {0 ~> strm_fvar x'}(strm_abs e) a n).
      set e'' := transl (strm_abs e ^ x') a n.
      assert (K: e' = e') by reflexivity.
      subst e'.
      rewrite[in X in X = _] transl_equation in K. simpl in K.
      simpl. rewrite <- K.
      apply_fresh typing_fix. subst e''.
      rewrite K.
      replace (strm_abs {1 ~> strm_fvar x'}e) with ((strm_abs e) ^ x')%s
        by reflexivity.
      rewrite~ (@close_var_rename_trm y).
      rewrite~ transl_rename.
      * rewrite~ <- close_var_expr_open.
        -- set T' := transl_typ (styp_arrow T1 T2).
           assert (Eq: T' = transl_typ (styp_arrow T1 T2)) by reflexivity.
           simpl in T'. subst T'. rewrite Eq.
           rewrite~ subst_open_strm.
           set T' := transl_typ (styp_arrow T1 T2).
           calc_open_subst.
           rewrite~ subst_fresh_strm; last by apply var_gen_spec.
           admit.
        -- rewrite~ subst_open_strm. calc_open_subst.
           rewrite~ subst_fresh_strm; last by apply var_gen_spec.
           apply~ transl_regular.
           replace (strm_abs {1 ~> strm_fvar y}e)%s with ((strm_abs e) ^ y)%s; last by reflexivity.
           apply* styping_regular_e.
      * forwards~ : (H y).
        rewrite~ (@subst_intro_strm y). simpls~.
      * apply notin_fv_open_strm; simpls~.
    + admit.
    + admit.                    (* RE *)
  - assert (K1: wf_env (map transl_typ G & 'a)).
    { apply~ wf_env_spush. apply~ transl_env_sound. }
    assert (K2: wf_typ (map transl_typ G & 'a) (transl_typ T)).
    { apply~ transl_typ_sound. }
    assert (K3: wf_name (map transl_typ G & 'a) n).
    { apply~ closed_name_wf. }
    set E0 := trm_kstar (trm_plus (trm_single trm_on) (trm_single trm_off)).
    apply typing_sub_E with
        (E1 := trm_dot (trm_single (trm_append (trm_nvar a) n))
                       (trm_plus (trm_dot (trm_single trm_on) E0)
                                 (trm_single trm_off))); subst E0.
    + apply~ typing_choice.
      * forwards : (IHTyping1 a (trm_append n trm_on)); auto with closed_name.
        apply (typing_sub_E H); auto 10. admit. (* RE *)
      * forwards : (IHTyping2 a (trm_append n trm_on)); auto with closed_name.
        apply (typing_sub_E H); auto 10. admit. (* RE *)
      * admit.                                  (* RE *)
      * admit.                                  (* disjoint *)
      * simpl. auto.
      * simpl. auto.
    + auto 10.
    + admit.                    (* RE *)
Admitted.
