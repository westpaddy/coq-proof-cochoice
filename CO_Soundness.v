Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From CO Require Import CO_Regularity.
From CO Require Import CO_RegExp.
From CO Require Import CO_Weakening.
From CO Require Import CO_TypingClosed.
From CO Require Import CO_TypingSubst.
From CO Require Import CO_Typing.

Lemma value_is_pure : forall v T E,
    empty |= v ~: T & E ->
    value v ->
    empty |= v ~: T & trm_empty.
Proof.
  intros* Typing. gen_eq G : (empty : env).
  induction Typing; intros* Eq Val; inversion Val; subst.
  - apply_fresh* typing_abs.
  - apply_fresh* typing_sabs.
  - eauto with eff_sub.
  - eauto with eff_sub.
Qed.

Lemma subject_red_distr_app_l : forall e1 e2 T1 T2 n E1 E2 E3,
    empty |= e1 ~: trm_arrow T1 (trm_dot (trm_single n) E1) T2 & E2 ->
    empty |= e2 ~: T1 & (trm_dot (trm_single n) E3) ->
    eff_disjoint E3 E1 ->
    eff_disjoint (trm_dot (trm_single n) E1) E2 ->
    eff_disjoint (trm_dot (trm_single n) E3) E2 ->
    empty |= trm_app e1 e2 ~: T2 &
    (trm_dot (trm_single trm_eps)
             (trm_dot (trm_single trm_eps)
                      (trm_plus (trm_dot (trm_single n) E1)
                                (trm_plus E2 (trm_dot (trm_single n) E3))))).
Proof.
  intros* H1 H2 Dis1 Dis2 Dis3.
  apply typing_sub_E with
      (E1 := trm_dot (trm_single trm_eps)
                     (trm_plus (trm_dot (trm_single n) E1)
                               (trm_plus E2 (trm_dot (trm_single n) E3))));
    auto 6 with eff_sub.
  apply typing_app with (T1 := T1); auto with eff_disjoint eff_sub.
  - apply* typing_sub; auto using typ_sub_refl with eff_sub.
  - apply* typing_sub; auto using typ_sub_refl with eff_sub.
  - intros. apply (@typing_closed_T a) in H1; last auto. simpls~.
  - intros. apply* typing_closed_E.
  - intros. apply* typing_closed_E.
Qed.

Lemma subject_red_distr_app_r : forall e1 e2 T1 T2 n E1 E2 E3,
    empty |= e1 ~: trm_arrow T1 (trm_dot (trm_single n) E1) T2 & (trm_dot (trm_single n) E2) ->
    empty |= e2 ~: T1 & E3 ->
    eff_disjoint E1 E2 ->
    eff_disjoint (trm_dot (trm_single n) E1) E3 ->
    eff_disjoint (trm_dot (trm_single n) E2) E3 ->
    empty |= trm_app e1 e2 ~: T2 &
    (trm_dot (trm_single trm_eps)
             (trm_dot (trm_single trm_eps)
                      (trm_plus (trm_dot (trm_single n) E1)
                                (trm_plus (trm_dot (trm_single n) E2) E3)))).
Proof.
  intros* H1 H2 Dis1 Dis2 Dis3.
  apply typing_sub_E with
      (E1 := trm_dot (trm_single trm_eps)
                     (trm_plus (trm_dot (trm_single n) E1)
                               (trm_plus (trm_dot (trm_single n) E2) E3)));
    auto 6 with eff_sub.
  apply typing_app with (T1 := T1); auto with eff_disjoint eff_sub.
  - apply* typing_sub; auto using typ_sub_refl with eff_sub.
  - apply* typing_sub; auto using typ_sub_refl with eff_sub.
  - intros. apply (@typing_closed_T a) in H1; last auto. simpls~.
  - intros. apply* typing_closed_E.
  - intros. apply* typing_closed_E.
Qed.

Lemma subject_red_distr_sapp : forall e T n n' E1 E2 E3,
  empty |= e ~: trm_forall E1 T & E2 ->
  wf_name empty n ->
  wf_effect empty (trm_dot (trm_single n') E3) ->
  eff_sub (E1 ^^ n) (trm_dot (trm_single n') E3) ->
  eff_disjoint (trm_dot (trm_single n') E3) E2 ->
  empty |= trm_sapp e n ~: (T ^^ n) &
  trm_dot (trm_single trm_eps)
          (trm_dot (trm_single trm_eps)
                   (trm_plus E2 (trm_dot (trm_single n') E3))).
Proof.
  intros* Typ Wfn WfE Sub Dis.
  apply typing_sub_E with
      (E1 := trm_dot (trm_single trm_eps)
                     (trm_plus E2 (trm_dot (trm_single n') E3)));
    auto with eff_sub.
  apply typing_sapp with (E1 := E1); auto with eff_disjoint eff_sub.
  - apply* typing_sub; auto using typ_sub_refl with eff_sub.
  - intros. apply* typing_closed_E.
  - intros. apply* wf_effect_closed.
Qed.

Lemma subject_red : forall D e e' T E,
    empty |= e ~: T & E ->
    e -->D e' ->
    empty |= e' ~: T & E.
Proof.
  intros* Typing. gen_eq G : (empty : env). move: D e'.
  induction Typing; intros* Eq Step; subst; try solve [inverts* Step].
  - inverts* Step.
    + move: (typing_abs_inv Typing1) => [L [T1' [K1 K2]]].
      pick_fresh y. rewrite~ (@subst_intro_trm y).
      apply_empty typing_subst.
      * apply* typing_sub_E; auto 6 with eff_sub.
        forwards~ : (K1 y).
        forwards~ :  (typing_presuppose_G H5).
        apply typing_presuppose_E in Typing1.
        forwards~ : (wf_effect_weaken_append Typing1 H6).
        apply typing_presuppose_E in Typing2.
        forwards~ : (wf_effect_weaken_append Typing2 H6).
      * apply typing_sub with (T1 := T1) (E1 := trm_empty); auto with eff_sub.
        -- apply* value_is_pure.
        -- forwards~ : (K1 y).
           apply typing_presuppose_G in H5.
           destruct (proj22 (wf_env_push_inv H5)).
           ++ assumption.
           ++ subst. inversion K2.
    + move: (typing_choice_inv Typing1)
      => [E1' [E2' [K1 [K2 [K3 [K4 [K5 [K6 K7]]]]]]]].
      apply typing_sub_E with
          (E1 := trm_dot (trm_single trm_eps)
                         (trm_plus
                            (trm_dot (trm_single trm_eps)
                                     (trm_plus
                                        (trm_dot (trm_single n) E1)
                                        (trm_plus E1'
                                                  (trm_dot (trm_single n) E3))))
                            E2'));
        auto.
      * apply typing_choice; auto using typ_sub_refl with eff_sub.
        -- apply* subject_red_distr_app_l.
           ++ eapply eff_disjoint_sub_R.
              ** apply* eff_disjoint_pre.
              ** apply* eff_sub_distr_L.
           ++ apply eff_disjoint_comm.
              apply* eff_disjoint_sub_L.
              ** apply* eff_disjoint_pre.
              ** apply* eff_sub_distr_L.
        -- apply* subject_red_distr_app_l.
           ++ eapply eff_disjoint_sub_R.
              ** apply* eff_disjoint_pre.
              ** apply* eff_sub_distr_L.
           ++ apply eff_disjoint_comm.
              eapply eff_disjoint_sub_L.
              ** apply* eff_disjoint_pre.
              ** apply* eff_sub_distr_L.
        -- apply eff_disjoint_sub_L with
               (E1 := trm_plus (trm_dot (trm_single n) E1)
                               (trm_plus E1' (trm_dot (trm_single n) E3)));
             auto with eff_sub.
           ++ apply eff_disjoint_plus.
              ** eapply eff_disjoint_sub_R.
                 --- apply* eff_disjoint_pre.
                 --- apply* eff_sub_distr_R.
              ** apply~ eff_disjoint_plus.
                 eapply eff_disjoint_sub_R; first last.
                 --- apply* eff_sub_distr_R.
                 --- apply~ eff_disjoint_pre. apply~ eff_disjoint_comm.
        -- intros.
           apply (@typing_closed_T a) in K1; last by auto.
           apply (@typing_closed_E a) in K2; last by auto.
           apply (@typing_closed_E a) in Typing2; last by auto.
           simpls~.
        -- intro. apply* wf_effect_closed.
      * apply eff_sub_dot_eps_L. apply eff_sub_plus.
        -- apply eff_sub_dot_eps_L. apply eff_sub_plus.
           ++ auto with eff_sub.
           ++ apply eff_sub_plus.
              ** admit.
              ** admit.
        -- admit.
    + move: (typing_choice_inv Typing2)
      => [E1' [E2' [K1 [K2 [K3 [K4 [K5 [K6 K7]]]]]]]].
      apply typing_sub_E with
          (E1 := trm_dot (trm_single trm_eps)
                         (trm_plus
                            (trm_dot (trm_single trm_eps)
                                     (trm_plus
                                        (trm_dot (trm_single n) E1)
                                        (trm_plus (trm_dot (trm_single n) E2) E1')))
                            E2'));
        auto.
      * apply typing_choice; auto using typ_sub_refl with eff_sub.
        -- apply* subject_red_distr_app_r.
           ++ apply eff_disjoint_comm.
              apply* eff_disjoint_sub_L.
              ** apply* eff_disjoint_pre.
              ** apply* eff_sub_distr_L.
           ++ eapply eff_disjoint_sub_R.
              ** apply* eff_disjoint_pre.
              ** apply* eff_sub_distr_L.
        -- apply* subject_red_distr_app_r.
           ++ apply eff_disjoint_comm.
              apply* eff_disjoint_sub_L.
              ** apply* eff_disjoint_pre.
              ** apply* eff_sub_distr_L.
           ++ eapply eff_disjoint_sub_R.
              ** apply* eff_disjoint_pre.
              ** apply* eff_sub_distr_L.
        -- eapply eff_disjoint_sub_L; first last.
           ++ apply eff_sub_dot_eps_L. apply~ eff_sub_refl.
           ++ apply eff_disjoint_plus.
              ** apply eff_disjoint_comm.
                 eapply eff_disjoint_sub_L.
                 --- apply* eff_disjoint_pre.
                 --- apply* eff_sub_distr_R.
              ** apply~ eff_disjoint_plus.
                 eapply eff_disjoint_sub_R; first last.
                 --- apply* eff_sub_distr_R.
                 --- apply~ eff_disjoint_pre.
        -- intros.
           apply (@typing_closed_E a) in K1; last by auto.
           apply (@typing_closed_E a) in Typing1; last by auto.
           simpl in *. auto.
        -- intros. apply* wf_effect_closed.
      * apply eff_sub_dot_eps_L. apply eff_sub_plus.
        -- apply eff_sub_dot_eps_L. apply eff_sub_plus.
           ++ admit.
           ++ admit.
        -- admit.
  - inverts* Step.
    + apply typing_sub_E with (E1 := (E1 ^^ n)); auto with eff_sub.
      move: (typing_sabs_inv Typing) => [L K].
      pick_fresh a. do! rewrite~ (@nsubst_intro_trm a n).
      apply_empty~ typing_nsubst.
    + move: (typing_choice_inv Typing)
      => [E1' [E2' [K1 [K2 [K3 [K4 [K5 [K6 K7]]]]]]]].
      apply typing_sub_E with
          (E1 := trm_dot (trm_single trm_eps)
                         (trm_plus
                            (trm_dot (trm_single trm_eps)
                                     (trm_plus E1' (trm_dot (trm_single n') E3)))
                            E2'));
        auto.
      * apply typing_choice; auto with eff_sub.
        -- apply* subject_red_distr_sapp.
           ++ eapply eff_disjoint_sub_R; first last.
              ** apply* eff_sub_distr_L.
              ** apply* eff_disjoint_pre. apply* eff_disjoint_comm.
        -- apply* subject_red_distr_sapp.
           ++ eapply eff_disjoint_sub_R; first last.
              ** apply* eff_sub_distr_L.
              ** apply* eff_disjoint_pre. apply* eff_disjoint_comm.
        -- eapply eff_disjoint_sub_L; first last.
           ++ apply eff_sub_dot_eps_L. apply~ eff_sub_refl.
           ++ apply~ eff_disjoint_plus.
              eapply eff_disjoint_sub_R.
              ** apply eff_disjoint_comm. apply* eff_disjoint_pre.
              ** apply* eff_sub_distr_R.
        -- intros.
           apply (@wf_effect_closed a) in H0; last auto.
           apply (@typing_closed_E a) in K1; last auto.
           simpl in *. auto.
        -- intros. apply* wf_effect_closed.
      * apply eff_sub_dot_eps_L. apply eff_sub_plus.
        -- apply eff_sub_dot_eps_L. apply eff_sub_plus.
           ++ admit.
           ++ admit.
        -- admit.
  - inverts* Step.
    assert (empty |= (trm_fix (trm_abs e)) ~: (trm_arrow T1 E T2) & trm_empty);
      first by eauto.
    pick_fresh f from (fv_trm (trm_abs e)). rewrite~ (@subst_intro_trm f).
    apply_empty* typing_subst.
  - inverts* Step.
    + apply* typing_sub; auto using typ_sub_refl with eff_sub.
    + apply* typing_sub; auto using typ_sub_refl with eff_sub.
Admitted.

Require Import Constructive_sets.

Lemma non_collapse : forall e1 e2 D T E,
    empty |= e1 ~: T & E ->
    e1 -->D e2 ->
    (forall n, inl n ∈ D \/ inr n ∈ D -> eff_disjoint (trm_single n) E) ->
    e1 ==> e2.
Proof.
  intros* Typing. gen_eq G : (empty : env). move: e2 D.
  induction Typing; intros* Eq Step Orth; subst; try solve [inverts* Step].
  - inverts* Step.
    + apply~ pstep_app_l. apply* IHTyping1.
      intros. move: (Orth n0 H5) => K.
      apply (eff_disjoint_sub_R K). auto 6 with eff_sub.
    + apply~ pstep_app_r. apply* IHTyping2.
      intros. move: (Orth n0 H5) => K.
      apply (eff_disjoint_sub_R K). auto 6 with eff_sub.
  - inverts* Step.
    apply~ pstep_sapp. apply* IHTyping.
    intros. move: (Orth n0 H5) => K.
    apply (eff_disjoint_sub_R K). auto with eff_sub.
  - inverts* Step.
    + apply~ pstep_choice_l. apply* IHTyping1.
      intros. destruct H5.
      * destruct (Add_inv _ _ _ _ H5).
        -- forwards~ : (Orth n0).
           apply (eff_disjoint_sub_R H7). auto with eff_sub.
        -- inverts H6. apply* eff_disjoint_sub_L.
           apply eff_disjoint_comm. apply* eff_disjoint_pre.
           apply effect_single_inv. apply (@effect_dot_inv (trm_single n') E2).
           auto.
      * destruct (Add_inv _ _ _ _ H5).
        -- forwards~ : (Orth n0).
           apply (eff_disjoint_sub_R H7). auto with eff_sub.
        -- inverts H6.
    + apply~ pstep_choice_r. apply* IHTyping2.
      intros. destruct H5.
      * destruct (Add_inv _ _ _ _ H5).
        -- forwards~ : (Orth n0).
           apply (eff_disjoint_sub_R H7). auto with eff_sub.
        -- inverts H6.
      * destruct (Add_inv _ _ _ _ H5).
        -- forwards~ : (Orth n0).
           apply (eff_disjoint_sub_R H7). auto with eff_sub.
        -- inverts H6. apply* eff_disjoint_sub_L.
           apply eff_disjoint_comm. apply* eff_disjoint_pre.
           apply effect_single_inv. apply (@effect_dot_inv (trm_single n') E2).
           auto.
    + forwards~ : (Orth n).
      false (eff_sub_disjoint_inv H1).
      apply (eff_disjoint_sub_R H5). auto with eff_sub.
    + forwards~ : (Orth n).
      false (eff_sub_disjoint_inv H1).
      apply (eff_disjoint_sub_R H5). auto with eff_sub.
  - apply* IHTyping. intros. apply* eff_disjoint_sub_R.
Qed.
