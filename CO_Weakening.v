Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From CO Require Import CO_Regularity.

Lemma wf_env_wf_name_wf_effect_wf_typ_weaken :
  (forall G, wf_env G -> forall G1 G2 x u,
        G = G1 & G2 -> (wf_typ G1 u \/ u = trm_star) -> x # G ->
        wf_env (G1 & x ~ u & G2)) /\
  (forall G n, wf_name G n -> forall G1 G2 x u,
        G = G1 & G2 -> (wf_typ G1 u \/ u = trm_star) -> x # G ->
        wf_name (G1 & x ~ u & G2) n) /\
  (forall G E, wf_effect G E -> forall G1 G2 x u,
        G = G1 & G2 -> (wf_typ G1 u \/ u = trm_star) -> x # G ->
        wf_effect (G1 & x ~ u & G2) E) /\
  (forall G T, wf_typ G T -> forall G1 G2 x u,
        G = G1 & G2 -> (wf_typ G1 u \/ u = trm_star) -> x # G ->
        wf_typ (G1 & x ~ u & G2) T).
Proof.
  apply wf_env_wf_name_wf_effect_wf_typ_ind; intros; subst*.
  - unpack (empty_concat_inv H); subst. rewrite~ concat_empty_r.
    destruct H0; subst~.
  - destruct G2 using env_ind; rew_env_concat in *; subst.
    + destruct H5; subst~.
    + unpack (eq_push_inv H4); subst*.
  - destruct G2 using env_ind; rew_env_concat in *; subst.
    + destruct H3; subst~.
    + unpack (eq_push_inv H2); subst*.
  - apply~ wf_name_var. apply~ binds_weaken.
  - apply_fresh wf_typ_forall.
    + apply_ih_bind* H0.
    + apply_ih_bind* H2.
Qed.

Lemma wf_env_weaken : forall G' G x u,
    wf_env (G & G') ->
    (wf_typ G u \/ u = trm_star) ->
    x # G & G' ->
    wf_env (G & x ~ u & G').
Proof. eauto using (proj41 wf_env_wf_name_wf_effect_wf_typ_weaken). Qed.

Lemma wf_name_weaken : forall G' G x u n,
    wf_name (G & G') n ->
    (wf_typ G u \/ u = trm_star) ->
    x # G & G' ->
    wf_name (G & x ~ u & G') n.
Proof. eauto using (proj42 wf_env_wf_name_wf_effect_wf_typ_weaken). Qed.

Lemma wf_effect_weaken : forall G' G x u E,
    wf_effect (G & G') E ->
    (wf_typ G u \/ u = trm_star) ->
    x # G & G' ->
    wf_effect (G & x ~ u & G') E.
Proof. eauto using (proj43 wf_env_wf_name_wf_effect_wf_typ_weaken). Qed.

Lemma wf_typ_weaken : forall G' G x u T,
    wf_typ (G & G') T ->
    (wf_typ G u \/ u = trm_star) ->
    x # G & G' ->
    wf_typ (G & x ~ u & G') T.
Proof. eauto using (proj44 wf_env_wf_name_wf_effect_wf_typ_weaken). Qed.

Local Hint Resolve wf_env_weaken wf_name_weaken wf_effect_weaken wf_typ_weaken.

Lemma typing_weaken : forall G' G x u e T E,
    G & G' |= e ~: T & E ->
    (wf_typ G u \/ u = trm_star) ->
    x # G & G' ->
    G & x ~ u & G' |= e ~: T & E.
Proof.
  intros* Typing. gen_eq GG : (G & G'). move: G G' x u.
  induction Typing; intros; subst; auto.
  - pose (wf_env_weaken H0 H2 H3). auto using binds_weaken.
  - apply* typing_app.
  - apply_fresh~ typing_abs. apply_ih_bind* H1.
  - apply* typing_sapp.
  - apply_fresh~ typing_sabs. apply_ih_bind* H0.
  - apply_fresh~ typing_fix. apply_ih_bind* H0.
  - apply* typing_sub.
Qed.

Lemma wf_name_weaken_append : forall G1 G2 n,
    wf_name G1 n ->
    wf_env (G1 & G2) ->
    wf_name (G1 & G2) n.
Proof.
  induction G2 using env_ind; intros; rew_env_concat in *.
  - done.
  - apply_empty wf_name_weaken.
    + apply~ IHG2. apply (wf_env_push_inv H0).
    + apply (wf_env_push_inv H0).
    + eapply ok_push_inv. eauto.
Qed.

Lemma wf_effect_weaken_append : forall G1 G2 E,
    wf_effect G1 E ->
    wf_env (G1 & G2) ->
    wf_effect (G1 & G2) E.
Proof.
  induction G2 using env_ind; intros; rew_env_concat in *.
  - done.
  - apply_empty wf_effect_weaken.
    + apply~ IHG2. apply (wf_env_push_inv H0).
    + apply (wf_env_push_inv H0).
    + eapply ok_push_inv. eauto.
Qed.

Lemma wf_typ_weaken_append : forall G1 G2 T,
    wf_typ G1 T ->
    wf_env (G1 & G2) ->
    wf_typ (G1 & G2) T.
Proof.
  induction G2 using env_ind; intros; rew_env_concat in *.
  - done.
  - apply_empty wf_typ_weaken.
    + apply~ IHG2. apply (wf_env_push_inv H0).
    + apply (wf_env_push_inv H0).
    + eapply ok_push_inv. eauto.
Qed.

Lemma typing_weaken_append : forall G1 G2 e T E,
    G1 |= e ~: T & E ->
    wf_env (G1 & G2) ->
    G1 & G2 |= e ~: T & E.
Proof.
  induction G2 using env_ind; intros; rew_env_concat in *.
  - done.
  - apply_empty typing_weaken.
    + apply~ IHG2. apply (wf_env_push_inv H0).
    + apply (wf_env_push_inv H0).
    + eapply ok_push_inv. eauto.
Qed.
