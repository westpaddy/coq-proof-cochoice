Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From CO Require Import CO_Regularity.
From CO Require Import CO_RegExp.
From CO Require Import CO_Weakening.
From CO Require Import CO_TypingSubst.

(** misc lemmas for typing relation *)

Lemma typ_sub_refl : forall T,
    typ T ->
    typ_sub T T.
Proof.
  induction 1.
  - apply typ_sub_nat.
  - apply~ typ_sub_arrow. apply~ eff_sub_refl.
  - apply_fresh* typ_sub_forall. apply~ eff_sub_refl.
Qed.

Lemma typ_sub_trans : forall T1 T2 T3,
    typ_sub T1 T2 ->
    typ_sub T2 T3 ->
    typ_sub T1 T3.
Proof.
  intros* H. move: (typ_sub_regular_R H) => K. move: T1 T3 H.
  induction K; intros.
  - inverts H. assumption.
  - inverts H0. inverts H1. apply* typ_sub_arrow. apply* eff_sub_trans.
  - inverts H2. inverts H3. apply_fresh typ_sub_forall.
    + forwards~ : (H1 y).
    + forwards~ : (H8 y). forwards~ : (H9 y). apply* eff_sub_trans.
Qed.

Lemma wf_effect_single_inv : forall G n,
    wf_effect G (trm_single n) ->
    wf_name G n.
Proof. inversion 1; auto. Qed.

Lemma wf_effect_dot_inv : forall G E1 E2,
    wf_effect G (trm_dot E1 E2) ->
    wf_effect G E1 /\ wf_effect G E2.
Proof. inversion 1; auto. Qed.

Lemma wf_effect_plus_inv : forall G E1 E2,
    wf_effect G (trm_plus E1 E2) ->
    wf_effect G E1 /\ wf_effect G E2.
Proof. inversion 1; auto. Qed.

Lemma wf_effect_star_inv : forall G E,
    wf_effect G (trm_kstar E) ->
    wf_effect G E.
Proof. inversion 1; auto. Qed.

Hint Extern 1 (wf_name ?G ?n) =>
match goal with
| [ H: context [trm_single ?n] |- _ ] => apply (@wf_effect_single_inv G n)
end.

Hint Extern 1 (wf_effect ?G ?E) =>
match goal with
| [ H: context [trm_dot E ?E'] |- _ ] => apply (@wf_effect_dot_inv G E E')
| [ H: context [trm_dot ?E' E] |- _ ] => apply (@wf_effect_dot_inv G E' E)
| [ H: context [trm_plus E ?E'] |- _ ] => apply (@wf_effect_plus_inv G E E')
| [ H: context [trm_plus ?E' E] |- _ ] => apply (@wf_effect_plus_inv G E' E)
| [ H: context [trm_kstar E] |- _ ] => apply (@wf_effect_star_inv G E)
end.

Lemma wf_typ_arrow_inv : forall G T1 T2 E,
    wf_typ G (trm_arrow T1 E T2) ->
    wf_typ G T1 /\ wf_typ G T2 /\ wf_effect G E.
Proof. inversion 1; auto. Qed.

Hint Extern 1 (wf_typ ?G ?T) =>
match goal with
| [ H: context [trm_arrow T ?E ?T'] |- _ ] => apply (@wf_typ_arrow_inv G T T' E)
| [ H: context [trm_arrow ?T' ?E T] |- _ ] => apply (@wf_typ_arrow_inv G T' T E)
end.

Hint Extern 1 (wf_effect ?G ?E) =>
match goal with
| [ H: context [trm_arrow ?T E ?T'] |- _ ] => apply (@wf_typ_arrow_inv G T T' E)
end.

Lemma typing_presuppose : forall G e T E,
    G |= e ~: T & E ->
    wf_env G /\ wf_typ G T /\ wf_effect G E.
Proof.
  induction 1; unpack; splits*.
  - destruct (wf_env_binds_inv H0 H1).
    + done.
    + subst. inversion H.
  - specialize_fresh. unpack. apply* wf_env_push_inv.
  - specialize_fresh. unpack. apply~ wf_typ_arrow.
    + apply_empty* wf_effect_subst.
    + destruct (proj22 (wf_env_push_inv H1)).
      * done.
      * subst. inversion H.
    + apply_empty* wf_typ_subst.
  - specialize_fresh. unpack. apply wf_effect_empty. apply* wf_env_push_inv.
  - inverts H7. pick_fresh a. rewrite~ (@nsubst_intro_trm a).
    apply_empty~ wf_typ_nsubst.
  - specialize_fresh. unpack. apply* wf_env_push_inv.
  - apply_fresh wf_typ_forall.
    + apply* H0.
    + apply* H0.
  - specialize_fresh. unpack. apply wf_effect_empty. apply* wf_env_push_inv.
  - specialize_fresh. unpack. apply* wf_env_push_inv.
  - specialize_fresh. unpack. destruct (proj22 (wf_env_push_inv H0)).
    + done.
    + inverts H3.
  - specialize_fresh. unpack. apply wf_effect_empty. apply* wf_env_push_inv.
Qed.

Lemma typing_presuppose_G : forall G e T E,
    G |= e ~: T & E ->
    wf_env G.
Proof. intros. apply (typing_presuppose H). Qed.

Lemma typing_presuppose_T : forall G e T E,
    G |= e ~: T & E ->
    wf_typ G T.
Proof. intros. apply (typing_presuppose H). Qed.

Lemma typing_presuppose_E : forall G e T E,
    G |= e ~: T & E ->
    wf_effect G E.
Proof. intros. apply (typing_presuppose H). Qed.

Hint Extern 1 (wf_env ?G) =>
match goal with
| [ H: G |= _ ~: _ & _ |- _ ] => exact (typing_presuppose_G H)
end.

Hint Extern 1 (wf_typ ?G ?T) =>
match goal with
| [ H: G |= _ ~: T & _ |- _ ] => exact (typing_presuppose_T H)
end.

Hint Extern 1 (wf_effect ?G ?E) =>
match goal with
| [ H: G |= _ ~: _ & E |- _ ] => exact (typing_presuppose_E H)
end.

Lemma typing_abs_inv : forall e1 T1 T2 E1 E2,
    empty |= (trm_abs e1) ~: (trm_arrow T1 E1 T2) & E2 ->
    exists L T1',
      (forall x, x \notin L -> empty & x ~ T1' |= e1 ^ x ~: T2 & E1) /\
      (typ_sub T1 T1').
Proof.
  intros* Typing.
  gen_eq G : (empty : env).
  gen_eq e : (trm_abs e1).
  gen_eq T : (trm_arrow T1 E1 T2).
  move: e1 T1 T2 E1.
  induction Typing; intros* EqT Eqe EqG; inversion Eqe; inversion EqT; subst.
  - exists L T0. splits*. apply~ typ_sub_refl.
  - inverts H1. forwards~ : IHTyping. move: H1 => [L [T1' [K1 K2]]].
    exists L T1'. splits*.
    + intros. forwards~ : (K1 x). apply~ typing_sub.
      * apply* wf_typ_weaken_append.
      * apply* wf_effect_weaken_append.
    + apply* typ_sub_trans.
Qed.

Lemma typing_sabs_inv : forall e1 T1 E1 E2,
    empty |= (trm_sabs e1) ~: (trm_forall E1 T1) & E2 ->
    exists L, (forall a, a \notin L -> empty & 'a |= e1 ^ 'a ~: (T1 ^ 'a) & (E1 ^ 'a)).
Proof.
  intros* Typing.
  gen_eq G : (empty : env).
  gen_eq e : (trm_sabs e1).
  gen_eq T : (trm_forall E1 T1).
  move: e1 T1 E1.
  induction Typing; intros* EqT Eqe EqG; inversion Eqe; inversion EqT; subst.
  - exists L. done.
  - inverts H1. forwards~ : IHTyping. move: H1 => [L' K]. inverts H.
    exists (L \u L' \u L0). intros.
    apply typing_sub with (T1 := (T2 ^ 'a)) (E1 := (E3 ^ 'a)); auto.
Qed.

Lemma typing_choice_inv : forall e1 e2 n T E,
    empty |= (trm_choice e1 n e2) ~: T & E ->
    exists E1 E2,
      (empty |= e1 ~: T & E1) /\
      (empty |= e2 ~: T & E1) /\
      (wf_name empty n) /\
      (wf_effect empty E2) /\
      (eff_sub (trm_single n) E2) /\
      (eff_disjoint E1 E2) /\
      (eff_sub (trm_plus E1 E2) E).
Proof.
  intros* Typing. gen_eq G : (empty : env). gen_eq e : (trm_choice e1 n e2).
  move: e1 e2 n.
  induction Typing; intros* Eqe EqG; inversion Eqe; subst.
  - exists (trm_dot (trm_single n') E1) (trm_dot (trm_single n') E2).
    splits; eauto with eff_disjoint eff_sub.
  - forwards~ : IHTyping; unpack.
    exists E0 E3. splits; eauto with eff_sub.
Qed.

Lemma typing_sub_E : forall G e T E1 E2,
    G |= e ~: T & E1 ->
    wf_effect G E2 ->
    eff_sub E1 E2 ->
    G |= e ~: T & E2.
Proof. intros. apply (typing_sub H); auto using typ_sub_refl. Qed.
