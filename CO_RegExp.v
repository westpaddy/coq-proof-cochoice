Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From CO Require Export CO_Regularity.

Axiom eff_disjoint_comm : forall E1 E2,
    eff_disjoint E1 E2 ->
    eff_disjoint E2 E1.

Axiom eff_disjoint_pre : forall n E1 E2,
    name n ->
    eff_disjoint E1 E2 ->
    eff_disjoint (trm_dot (trm_single n) E1) (trm_dot (trm_single n) E2).

Axiom eff_disjoint_plus : forall E1 E2 E,
    eff_disjoint E1 E ->
    eff_disjoint E2 E ->
    eff_disjoint (trm_plus E1 E2) E.

Axiom eff_disjoint_sub_L : forall E1 E2 E',
    eff_disjoint E1 E2 ->
    eff_sub E' E1 ->
    eff_disjoint E' E2.

Axiom eff_disjoint_sub_R : forall E1 E2 E',
    eff_disjoint E1 E2 ->
    eff_sub E' E2 ->
    eff_disjoint E1 E'.

Hint Resolve
     eff_disjoint_comm eff_disjoint_pre eff_disjoint_plus
     eff_disjoint_sub_L eff_disjoint_sub_R : eff_disjoint.

Axiom eff_sub_refl : forall E,
    effect E ->
    eff_sub E E.

Axiom eff_sub_trans : forall E1 E2 E3,
    eff_sub E1 E2 ->
    eff_sub E2 E3 ->
    eff_sub E1 E3.

Axiom eff_sub_nsubst : forall a n E1 E2,
    eff_sub E1 E2 ->
    eff_sub ['a ~> n]E1 ['a ~> n]E2.

Axiom eff_sub_dot_eps_L : forall E1 E2,
    eff_sub E1 E2 ->
    eff_sub (trm_dot (trm_single trm_eps) E1) E2.

Axiom eff_sub_dot_eps_R : forall E1 E2,
    eff_sub E1 E2 ->
    eff_sub E1 (trm_dot (trm_single trm_eps) E2).

Axiom eff_sub_plus : forall E1 E2 E,
    eff_sub E1 E ->
    eff_sub E2 E ->
    eff_sub (trm_plus E1 E2) E.

Axiom eff_sub_plus_L : forall E1 E2 E,
    effect E ->
    eff_sub E1 E2 ->
    eff_sub E1 (trm_plus E2 E).

Axiom eff_sub_plus_R : forall E1 E2 E,
    effect E ->
    eff_sub E1 E2 ->
    eff_sub E1 (trm_plus E E2).

Axiom eff_sub_distr_L : forall E1 E2 E,
    eff_sub (trm_plus E1 E2) E ->
    eff_sub E1 E.

Axiom eff_sub_distr_R : forall E1 E2 E,
    eff_sub (trm_plus E1 E2) E ->
    eff_sub E2 E.

Axiom eff_sub_dot_plus : forall E1 E2 E3,
    effect E1 ->
    effect E2 ->
    effect E3 ->
    eff_sub (trm_dot E1 (trm_plus E2 E3))
            (trm_plus (trm_dot E1 E2) (trm_dot E1 E3)).

Axiom eff_sub_plus_dot : forall E1 E2 E3,
    effect E1 ->
    effect E2 ->
    effect E3 ->
    eff_sub (trm_plus (trm_dot E1 E2) (trm_dot E1 E3))
            (trm_dot E1 (trm_plus E2 E3)).

Hint Resolve
     eff_sub_refl eff_sub_trans eff_sub_dot_eps_L eff_sub_dot_eps_R
     eff_sub_plus eff_sub_plus_L eff_sub_plus_R
     eff_sub_distr_L eff_sub_distr_R
     eff_sub_dot_plus eff_sub_plus_dot : eff_sub.

Hint Extern 1 (eff_sub _ (trm_dot ?K1 (trm_plus ?K2 ?K3))) =>
apply eff_sub_trans with (E2 := trm_plus (trm_dot K1 K2) (trm_dot K1 K3)) : eff_sub.

Axiom eff_sub_disjoint_inv : forall E1 E2,
    eff_sub E1 E2 ->
    eff_disjoint E1 E2 ->
    False.
