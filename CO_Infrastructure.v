Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From CO Require Export CO_Definition.

(** ** tactics dealing variables *)

Ltac gather_vars :=
  let A := gather_vars_with (fun x : var => \{x}) in
  let B := gather_vars_with (fun x : vars => x) in
  let C := gather_vars_with (fun x : trm => fv_trm x \u fnv_trm x) in
  let D := gather_vars_with (fun x : env => dom x) in
  let E := gather_vars_with (fun x : strm => fv_strm x) in
  let F := gather_vars_with (fun x : senv => dom x) in
  constr:(A \u B \u C \u D \u E \u F).

Tactic Notation "pick_fresh" ident(x) :=
  let L := gather_vars in (pick_fresh_gen L x).

Tactic Notation "pick_fresh" ident(x) "from" constr(E) :=
  let L := gather_vars in (pick_fresh_gen (L \u E) x).

Tactic Notation "apply_fresh" constr(H) :=
  apply_fresh_base H gather_vars ltac_no_arg.

Tactic Notation "apply_fresh" "~" constr(H) :=
  apply_fresh H; auto_tilde.

Tactic Notation "apply_fresh" "*" constr(H) :=
  apply_fresh H; auto_star.

Ltac calc_open_subst :=
  unfold open_trm, open_strm; repeat (simpl; first [case_nat | case_var]); simpl.

Tactic Notation "calc_open_subst" "in" constr(H) :=
  unfold open_trm, open_strm in H; repeat (simpl in H; first [case_nat | case_var]); simpl in H.

Ltac specialize_fresh_base H :=
  match type of H with
  | forall x, x \notin ?L -> _ =>
    let y := fresh "y" in
    let NotIn := fresh "H" in
    pick_fresh y;
    (assert (NotIn: y \notin L) by notin_solve);
    specialize (H y NotIn);
    clear NotIn
  end.

Tactic Notation "specialize_fresh" constr(H) :=
  specialize_fresh_base H.

Tactic Notation "specialize_fresh" :=
  repeat (match goal with
          | [ H : forall x, x \notin _ -> _ |- _ ] =>
            specialize_fresh_base H
          end).

Scheme Minimality for wf_env Sort Prop
  with Minimality for wf_name Sort Prop
  with Minimality for wf_effect Sort Prop
  with Minimality for wf_typ Sort Prop.
Combined Scheme wf_env_wf_name_wf_effect_wf_typ_ind from wf_env_ind, wf_name_ind, wf_effect_ind, wf_typ_ind.

Hint Constructors
     sexpr styping
     name effect typ expr value environment
     step wf_env wf_name wf_effect wf_typ typ_sub typing pstep.

Section Slang.

Local Open Scope s_scope.

Lemma open_sterm_rec : forall j v t i u,
    {j ~> v}t = {i ~> u}({j ~> v}t) ->
    i <> j ->
    t = {i ~> u}t.
Proof.
  move=> j v t. move: j v.
  induction t; simpl; introv Eq Neq; inverts Eq; f_equal*.
  - case_nat~. case_nat~.
Qed.

Lemma open_sexpr : forall k u e,
    sexpr e ->
    e = {k ~> u}e.
Proof.
  intros * Exp. move: k u.
  induction Exp; simpl; intros; f_equal;
    specialize_fresh; eauto using open_sterm_rec.
Qed.

Lemma subst_open_strm_rec : forall x e v t k,
    sexpr e ->
    [x ~> e]({k ~> v}t) = {k ~> [x ~> e]v}([x ~> e]t).
Proof.
  induction t; intros; calc_open_subst; f_equal~.
  - by apply open_sexpr.         (* case [t = x] *)
Qed.

Lemma subst_open_strm : forall x e v t,
    sexpr e ->
    [x ~> e](t ^^ v) = ([x ~> e]t) ^^ ([x ~> e]v).
Proof. intros. by apply subst_open_strm_rec. Qed.

Lemma subst_open_var_strm : forall x y e t,
    sexpr e ->
    x <> y ->
    [x ~> e](t ^ y) = ([x ~> e]t) ^ y.
Proof. intros. rewrite~ subst_open_strm. by calc_open_subst. Qed.

Lemma subst_fresh_strm : forall x u t,
    x \notin (fv_strm t) ->
    [x ~> u]t = t.
Proof. induction t; simpl; intros; calc_open_subst; f_equal~. Qed.

Lemma subst_intro_strm : forall x e t,
    x \notin (fv_strm t) ->
    sexpr e ->
    t ^^ e = [x ~> e](t ^ x).
Proof.
  intros. rewrite~ subst_open_strm. calc_open_subst. by rewrite subst_fresh_strm.
Qed.

End Slang.

(** * common properties for locally-closed terms *)

Lemma open_term_rec : forall j v t i u,
    {j ~> v}t = {i ~> u}({j ~> v}t) ->
    i <> j ->
    t = {i ~> u}t.
Proof.
  move=> j v t. move: j v.
  induction t; simpl; introv Eq Neq; inverts Eq; f_equal*.
  - case_nat~. case_nat~.
Qed.

Lemma open_name : forall k u n,
    name n ->
    n = {k ~> u}n.
Proof. induction 1; simpl; congruence. Qed.

Lemma open_effect : forall k u E,
    effect E ->
    E = {k ~> u}E.
Proof. induction 1; simpl; f_equal; by auto using open_name. Qed.

Lemma open_typ : forall k u T,
    typ T ->
    T = {k ~> u}T.
Proof.
  intros * Typ. move: k u.
  induction Typ; simpl; intros; f_equal;
    specialize_fresh; eauto using open_term_rec, open_effect.
Qed.

Lemma open_expr : forall k u e,
    expr e ->
    e = {k ~> u}e.
Proof.
  intros * Exp. move: k u.
  induction Exp; simpl; intros; f_equal;
    specialize_fresh; eauto using open_term_rec, open_name.
Qed.

Lemma subst_open_trm_rec : forall x e v t k,
    expr e ->
    [x ~> e]({k ~> v}t) = {k ~> [x ~> e]v}([x ~> e]t).
Proof.
  induction t; intros; calc_open_subst; f_equal~.
  - by apply open_expr.         (* case [t = x] *)
Qed.

Lemma subst_open_trm : forall x e v t,
    expr e ->
    [x ~> e](t ^^ v) = ([x ~> e]t) ^^ ([x ~> e]v).
Proof. intros. by apply subst_open_trm_rec. Qed.

Lemma nsubst_open_trm_rec : forall a n v t k,
    name n ->
    ['a ~> n]({k ~> v}t) = {k ~> ['a ~> n]v}(['a ~> n]t).
Proof.
  induction t; intros; calc_open_subst; f_equal~.
  - by apply open_name.         (* case [t = 'a] *)
Qed.

Lemma nsubst_open_trm : forall a n v t,
    name n ->
    ['a ~> n](t ^^ v) = (['a ~> n]t) ^^ (['a ~> n]v).
Proof. intros. by apply nsubst_open_trm_rec. Qed.

Lemma subst_open_var_trm : forall x y e t,
    expr e ->
    x <> y ->
    [x ~> e](t ^ y) = ([x ~> e]t) ^ y.
Proof. intros. rewrite~ subst_open_trm. by calc_open_subst. Qed.

Lemma subst_open_nvar_trm : forall x a e t,
    expr e ->
    [x ~> e](t ^ 'a) = ([x ~> e]t) ^ 'a.
Proof. intros. rewrite~ subst_open_trm. Qed.

Lemma nsubst_open_var_trm : forall a x n t,
    name n ->
    ['a ~> n](t ^ x) = (['a ~> n]t) ^ x.
Proof. intros. rewrite~ nsubst_open_trm. Qed.

Lemma nsubst_open_nvar_trm : forall a b n t,
    name n ->
    a <> b ->
    ['a ~> n](t ^ 'b) = (['a ~> n]t) ^ 'b.
Proof. intros. rewrite~ nsubst_open_trm. by calc_open_subst. Qed.

Lemma subst_fresh_trm : forall x u t,
    x \notin (fv_trm t) ->
    [x ~> u]t = t.
Proof. induction t; simpl; intros; calc_open_subst; f_equal~. Qed.

Lemma subst_intro_trm : forall x e t,
    x \notin (fv_trm t) ->
    expr e ->
    t ^^ e = [x ~> e](t ^ x).
Proof.
  intros. rewrite~ subst_open_trm. calc_open_subst. by rewrite subst_fresh_trm.
Qed.

Lemma nsubst_fresh_trm : forall a u t,
    a \notin (fnv_trm t) ->
    ['a ~> u]t = t.
Proof. induction t; simpl; intros; calc_open_subst; f_equal~. Qed.

Lemma nsubst_intro_trm : forall a n t,
    a \notin (fnv_trm t) ->
    name n ->
    t ^^ n = ['a ~> n](t ^ 'a).
Proof.
  intros. rewrite~ nsubst_open_trm. calc_open_subst. by rewrite nsubst_fresh_trm.
Qed.

(* lemmas for free variables *)

Lemma notin_fnv_open : forall a u t,
    a \notin fnv_trm u ->
    a \notin fnv_trm t ->
    a \notin fnv_trm (t ^^ u).
Proof.
  intros*. calc_open_subst. generalize 0.
  induction t; simpl; intros; calc_open_subst; auto.
Qed.

Hint Resolve notin_fnv_open.

Section Notin_Open.

Local Ltac solv :=
  let t := fresh "t" in
  intros ? ? t; calc_open_subst; generalize 0;
  induction t; simpl; intros;
    do ? [match goal with
            [ H: _ \notin _ \u _ |- _ ] => unpack (notin_union_r H); clear H
          end];
    eauto.

Lemma notin_fv_open_var : forall y x t,
    x <> y ->
    x \notin fv_trm (t ^ y) ->
    x \notin fv_trm t.
Proof. solv. Qed.

Lemma notin_fv_open_nvar : forall a x t,
    x \notin fv_trm (t ^ 'a) ->
    x \notin fv_trm t.
Proof. solv. Qed.

Lemma notin_fnv_open_nvar : forall b a t,
    a <> b ->
    a \notin fnv_trm (t ^ 'b) ->
    a \notin fnv_trm t.
Proof. solv. Qed.

Lemma notin_fnv_open_var : forall y a t,
    a \notin fnv_trm (t ^ y) ->
    a \notin fnv_trm t.
Proof. solv. Qed.

End Notin_Open.

Lemma notin_fv_name : forall x n,
    name n ->
    x \notin fv_trm n.
Proof. induction 1; simpl; auto. Qed.

Hint Resolve notin_fv_name.

Lemma notin_fv_effect : forall x E,
    effect E ->
    x \notin fv_trm E.
Proof. induction 1; simpl; auto. Qed.

Hint Resolve notin_fv_effect.

Lemma notin_fv_typ : forall x T,
    typ T ->
    x \notin fv_trm T.
Proof.
  induction 1; simpl; auto.
  - pick_fresh a. notin_simpl.
    + apply~ (@notin_fv_open_nvar a).
    + apply~ (@notin_fv_open_nvar a).
Qed.

Hint Resolve notin_fv_typ.
