Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From CO Require Import CO_Regularity.

Inductive closed_env : env -> Prop :=
| closed_env_empty :
    closed_env empty
| closed_env_push : forall G x u,
    closed_env G ->
    (forall a, a # G -> a \notin fnv_trm u) ->
    closed_env (G & x ~ u).

Lemma closed_env_push_inv_G : forall G x T,
    closed_env (G & x ~ T) ->
    closed_env G.
Proof.
  inversion 1.
  - false (empty_push_inv H1).
  - unpack (eq_push_inv H0). subst~.
Qed.

Lemma closed_env_push_inv_T : forall a G x T,
    closed_env (G & x ~ T) ->
    a # G ->
    a \notin fnv_trm T.
Proof.
  inversion 1; subst.
  - false (empty_push_inv H1).
  - unpack (eq_push_inv H0); subst~.
Qed.

Local Hint Constructors closed_env.
Local Hint Resolve closed_env_push_inv_G closed_env_push_inv_T.

Lemma wf_env_wf_name_wf_effect_wf_typ_closed :
  (forall G, wf_env G -> closed_env G) /\
  (forall G n, wf_name G n -> forall a, a # G -> a \notin fnv_trm n) /\
  (forall G E, wf_effect G E -> forall a, a # G -> a \notin fnv_trm E) /\
  (forall G T, wf_typ G T -> forall a, a # G -> a \notin fnv_trm T).
Proof.
  apply wf_env_wf_name_wf_effect_wf_typ_ind; simpl; intros; auto.
  - apply~ closed_env_push. simpl. auto.
  - apply notin_singleton. intro. subst. false (binds_fresh_inv H1 H2).
  - specialize_fresh. notin_simpl.
    + apply~ (@notin_fnv_open_nvar y1).
    + apply~ (@notin_fnv_open_nvar y).
Qed.

Lemma wf_env_closed : forall G,
    wf_env G ->
    closed_env G.
Proof. eauto using (proj41 wf_env_wf_name_wf_effect_wf_typ_closed). Qed.

Lemma wf_name_closed : forall a G n,
    wf_name G n ->
    a # G ->
    a \notin fnv_trm n.
Proof. eauto using (proj42 wf_env_wf_name_wf_effect_wf_typ_closed). Qed.

Lemma wf_effect_closed : forall a G E,
    wf_effect G E ->
    a # G ->
    a \notin fnv_trm E.
Proof. eauto using (proj43 wf_env_wf_name_wf_effect_wf_typ_closed). Qed.

Lemma wf_typ_closed : forall a G T,
    wf_typ G T ->
    a # G ->
    a \notin fnv_trm T.
Proof. eauto using (proj44 wf_env_wf_name_wf_effect_wf_typ_closed). Qed.

Local Hint Resolve wf_env_closed wf_name_closed wf_effect_closed wf_typ_closed.

Lemma typing_closed_e : forall a G e T E,
    G |= e ~: T & E ->
    a # G ->
    a \notin fnv_trm e.
Proof.
  induction 1; simpl; intros; eauto using wf_name_closed.
  - specialize_fresh. apply~ (@notin_fnv_open_var y).
  - specialize_fresh. apply~ (@notin_fnv_open_nvar y).
  - rewrite (_: fnv_trm e = fnv_trm (trm_abs e));
      first by reflexivity.
    specialize_fresh. apply~ (@notin_fnv_open_var y).
Qed.

Lemma typing_closed_TE : forall G e T E,
    G |= e ~: T & E ->
    (closed_env G) /\
    (forall a, a # G -> a \notin fnv_trm T) /\
    (forall a, a # G -> a \notin fnv_trm E).
Proof.
  induction 1; simpl in *; splits; intros; specialize_fresh; unpack; eauto.
  - induction G using env_ind.
    + false (binds_empty_inv H1).
    + binds_cases H1; subst.
      * apply~ IHG. apply* wf_env_push_inv.
      * apply* closed_env_push_inv_T.
  - forwards~ : (H12 a).
  - forwards~ : (H12 a).
  - apply* notin_fnv_open. forwards~ : (H8 a).
  - forwards~ : (H9 a).
  - notin_simpl.
    + apply~ (@notin_fnv_open_nvar y).
    + apply~ (@notin_fnv_open_nvar y).
  - forwards~ : (H10 a).
Qed.

Lemma typing_closed_T : forall a G e T E,
    G |= e ~: T & E ->
    a # G ->
    a \notin fnv_trm T.
Proof. intros. eauto using (proj32 (typing_closed_TE H)). Qed.

Lemma typing_closed_E : forall a G e T E,
    G |= e ~: T & E ->
    a # G ->
    a \notin fnv_trm E.
Proof. intros. eauto using (proj33 (typing_closed_TE H)). Qed.
