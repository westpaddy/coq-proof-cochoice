Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Export Ensembles.
From TLC Require Export LibLN.

Inductive styp : Set :=
| styp_nat : styp
| styp_arrow : styp -> styp -> styp
.

Definition senv := (LibEnv.env styp).

Inductive strm : Set :=
| strm_bvar : nat -> strm
| strm_fvar : var -> strm
| strm_app : strm -> strm -> strm
| strm_abs : strm -> strm
| strm_fix : strm -> strm
| strm_choice : strm -> strm -> strm
.

Bind Scope s_scope with strm.
Delimit Scope s_scope with s.

Reserved Notation "{ k ~> u } t" (at level 0, k at level 99, t at level 0).
Fixpoint open_strm_rec (k : nat) (u : strm) (t : strm) {struct t} : strm :=
  match t with
  | strm_bvar i => If i = k then u else strm_bvar i
  | strm_fvar x => strm_fvar x
  | strm_app t1 t2 => strm_app ({k ~> u}t1) ({k ~> u}t2)
  | strm_abs t1 => strm_abs ({S k ~> u}t1)
  | strm_fix t1 => strm_fix ({S k ~> u}t1)
  | strm_choice t1 t2 => strm_choice ({k ~> u}t1) ({k ~> u}t2)
  end
where "{ k ~> u } t" := (open_strm_rec k u t) : s_scope.

Definition open_strm t u := ({0 ~> u}t)%s.
Notation "t ^^ u" := (open_strm t u) (at level 67) : s_scope.
Notation "t ^ x" := (t ^^ (strm_fvar x))%s : s_scope.

Fixpoint fv_strm (t : strm) {struct t} : vars :=
  match t with
  | strm_bvar i => \{}
  | strm_fvar x => \{x}
  | strm_app t1 t2 => (fv_strm t1) \u (fv_strm t2)
  | strm_abs t1 => fv_strm t1
  | strm_fix t1 => fv_strm t1
  | strm_choice t1 t2 => (fv_strm t1) \u (fv_strm t2)
  end
.

Reserved Notation "[ z ~> u ] t" (at level 0, t at level 0).
Fixpoint subst_strm (z : var) (u : strm) (t : strm) {struct t} : strm :=
  match t with
  | strm_bvar i => strm_bvar i
  | strm_fvar x => If x = z then u else strm_fvar x
  | strm_app t1 t2 => strm_app ([z ~> u]t1) ([z ~> u]t2)
  | strm_abs t1 => strm_abs ([z ~> u]t1)
  | strm_fix t1 => strm_fix ([z ~> u]t1)
  | strm_choice t1 t2 => strm_choice ([z ~> u]t1) ([z ~> u]t2)
  end
where "[ z ~> u ] t" := (subst_strm z u t) : s_scope
.

Inductive sexpr : strm -> Prop :=
| sexpr_var : forall x,
    sexpr (strm_fvar x)
| sexpr_app : forall e1 e2,
    sexpr e1 ->
    sexpr e2 ->
    sexpr (strm_app e1 e2)
| sexpr_abs : forall L e,
    (forall x, x \notin L -> sexpr (e ^ x)) ->
    sexpr (strm_abs e)
| sexpr_fix : forall L e,
    (forall f, f \notin L -> sexpr (e ^ f)) ->
    sexpr (strm_fix e)
| sexpr_choice : forall e1 e2,
    sexpr e1 ->
    sexpr e2 ->
    sexpr (strm_choice e1 e2)
.

Reserved Notation "G |= e ~: T" (at level 69, T at level 27).
Inductive styping : senv -> strm -> styp -> Prop :=
| styping_var : forall G x T,
    ok G ->
    binds x T G ->
    G |= strm_fvar x ~: T
| styping_app : forall G e1 e2 T1 T2,
    G |= e1 ~: styp_arrow T1 T2 ->
    G |= e2 ~: T1 ->
    G |= strm_app e1 e2 ~: T2
| styping_abs : forall L G e T1 T2,
    (forall x, x \notin L -> G & x ~ T1 |= e ^ x ~: T2) ->
    G |= strm_abs e ~: styp_arrow T1 T2
| styping_fix : forall L G e T1 T2,
    (forall f, f \notin L ->
          G & f ~ styp_arrow T1 T2 |= (strm_abs e) ^ f ~: styp_arrow T1 T2) ->
    G |= strm_fix (strm_abs e) ~: styp_arrow T1 T2
| styping_choice : forall G e1 e2 T,
    G |= e1 ~: T ->
    G |= e2 ~: T ->
    G |= strm_choice e1 e2 ~: T
where "G |= e ~: T" := (styping G e T) : s_scope
.

(** ** Raw terms *)

Inductive trm : Set :=
(* term for a bound variable *)
| trm_bvar : nat -> trm
(* terms for effects *)
| trm_empty : trm
| trm_single : trm -> trm
| trm_dot : trm -> trm -> trm
| trm_plus : trm -> trm -> trm
| trm_kstar : trm -> trm
(* term for the kind for names *)
| trm_star : trm
(* terms for types *)
| trm_nat : trm
| trm_arrow : trm -> trm -> trm -> trm
| trm_forall : trm -> trm -> trm
(* terms for names *)
| trm_nvar : var -> trm
| trm_eps : trm
| trm_on : trm
| trm_off : trm
| trm_append : trm -> trm -> trm
(* terms for expressions *)
| trm_fvar : var -> trm
| trm_app : trm -> trm -> trm
| trm_abs : trm -> trm
| trm_sapp : trm -> trm -> trm
| trm_sabs : trm -> trm
| trm_fix : trm -> trm
| trm_choice : trm -> trm -> trm -> trm
.

Definition env := LibEnv.env trm.
Notation "G & ' a" := (G & a ~ trm_star) (at level 28).

Definition axis := (trm + trm)%type.
Definition orth := Ensemble axis.
Notation "∅" := (Empty_set axis).
Notation "D ⊌ x" := (Add axis D x) (at level 52, left associativity).
Notation "D1 ∪ D2" := (Union axis D1 D2) (at level 53, right associativity).
Notation "D1 ⊆ D2" := (Included axis D1 D2) (at level 54).
Notation "x ∈ D" := (In axis D x) (at level 55).
Notation "x ∉ D" := (~ (x ∈ D)) (at level 55).

(** ** Opening operation *)

Reserved Notation "{ k ~> u } t" (at level 0, k at level 99, t at level 0).
Fixpoint open_trm_rec (k : nat) (u : trm) (t : trm) {struct t} : trm :=
  match t with
  | trm_bvar i => If i = k then u else trm_bvar i
  | trm_empty => trm_empty
  | trm_single t1 => trm_single ({k ~> u}t1)
  | trm_dot t1 t2 => trm_dot ({k ~> u}t1) ({k ~> u}t2)
  | trm_plus t1 t2 => trm_plus ({k ~> u}t1) ({k ~> u}t2)
  | trm_kstar t1 => trm_kstar ({k ~> u}t1)
  | trm_star => trm_star
  | trm_nat => trm_nat
  | trm_arrow t1 t2 t3 => trm_arrow ({k ~> u}t1) ({k ~> u}t2) ({k ~> u}t3)
  | trm_forall t1 t2 => trm_forall ({S k ~> u}t1) ({S k ~> u}t2)
  | trm_nvar x => trm_nvar x
  | trm_eps => trm_eps
  | trm_on => trm_on
  | trm_off => trm_off
  | trm_append t1 t2 => trm_append ({k ~> u}t1) ({k ~> u}t2)
  | trm_fvar x => trm_fvar x
  | trm_app t1 t2 => trm_app ({k ~> u}t1) ({k ~> u}t2)
  | trm_abs t1 => trm_abs ({S k ~> u}t1)
  | trm_sapp t1 t2 => trm_sapp ({k ~> u}t1) ({k ~> u}t2)
  | trm_sabs t1 => trm_sabs ({S k ~> u}t1)
  | trm_fix t1 => trm_fix ({S k ~> u}t1)
  | trm_choice t1 t2 t3 => trm_choice ({k ~> u}t1) ({k ~> u}t2) ({k ~> u}t3)
  end
where "{ k ~> u } t" := (open_trm_rec k u t)
.

Definition open_trm t u := ({0 ~> u}t).
Notation "t ^^ u" := (open_trm t u) (at level 67).
Notation "t ^ x" := (t ^^ (trm_fvar x)).
Notation "t ^ ' a" := (t ^^ (trm_nvar a)) (at level 30).

Fixpoint fv_trm (t : trm) {struct t} : vars :=
  match t with
  | trm_bvar i => \{}
  | trm_empty => \{}
  | trm_single t1 => fv_trm t1
  | trm_dot t1 t2 => (fv_trm t1) \u (fv_trm t2)
  | trm_plus t1 t2 => (fv_trm t1) \u (fv_trm t2)
  | trm_kstar t1 => fv_trm t1
  | trm_star => \{}
  | trm_nat => \{}
  | trm_arrow t1 t2 t3 => (fv_trm t1) \u (fv_trm t2) \u (fv_trm t3)
  | trm_forall t1 t2 => (fv_trm t1) \u (fv_trm t2)
  | trm_nvar x => \{}
  | trm_eps => \{}
  | trm_on => \{}
  | trm_off => \{}
  | trm_append t1 t2 => (fv_trm t1) \u (fv_trm t2)
  | trm_fvar x => \{x}
  | trm_app t1 t2 => (fv_trm t1) \u (fv_trm t2)
  | trm_abs t1 => fv_trm t1
  | trm_sapp t1 t2 => (fv_trm t1) \u (fv_trm t2)
  | trm_sabs t1 => fv_trm t1
  | trm_fix t1 => fv_trm t1
  | trm_choice t1 t2 t3 => (fv_trm t1) \u (fv_trm t2) \u (fv_trm t3)
  end
.

Fixpoint fnv_trm (t : trm) {struct t} : vars :=
  match t with
  | trm_bvar i => \{}
  | trm_empty => \{}
  | trm_single t1 => fnv_trm t1
  | trm_dot t1 t2 => (fnv_trm t1) \u (fnv_trm t2)
  | trm_plus t1 t2 => (fnv_trm t1) \u (fnv_trm t2)
  | trm_kstar t1 => fnv_trm t1
  | trm_star => \{}
  | trm_nat => \{}
  | trm_arrow t1 t2 t3 => (fnv_trm t1) \u (fnv_trm t2) \u (fnv_trm t3)
  | trm_forall t1 t2 => (fnv_trm t1) \u (fnv_trm t2)
  | trm_nvar x => \{x}
  | trm_eps => \{}
  | trm_on => \{}
  | trm_off => \{}
  | trm_append t1 t2 => (fnv_trm t1) \u (fnv_trm t2)
  | trm_fvar x => \{}
  | trm_app t1 t2 => (fnv_trm t1) \u (fnv_trm t2)
  | trm_abs t1 => fnv_trm t1
  | trm_sapp t1 t2 => (fnv_trm t1) \u (fnv_trm t2)
  | trm_sabs t1 => fnv_trm t1
  | trm_fix t1 => fnv_trm t1
  | trm_choice t1 t2 t3 => (fnv_trm t1) \u (fnv_trm t2) \u (fnv_trm t3)
  end
.

Reserved Notation "[ z ~> u ] t" (at level 0, t at level 0).
Fixpoint subst_trm (z : var) (u : trm) (t : trm) {struct t} : trm :=
  match t with
  | trm_bvar i => trm_bvar i
  | trm_empty => trm_empty
  | trm_single t1 => trm_single ([z ~> u]t1)
  | trm_dot t1 t2 => trm_dot ([z ~> u]t1) ([z ~> u]t2)
  | trm_plus t1 t2 => trm_plus ([z ~> u]t1) ([z ~> u]t2)
  | trm_kstar t1 => trm_kstar ([z ~> u]t1)
  | trm_star => trm_star
  | trm_nat => trm_nat
  | trm_arrow t1 t2 t3 => trm_arrow ([z ~> u]t1) ([z ~> u]t2) ([z ~> u]t3)
  | trm_forall t1 t2 => trm_forall ([z ~> u]t1) ([z ~> u]t2)
  | trm_nvar x => trm_nvar x
  | trm_eps => trm_eps
  | trm_on => trm_on
  | trm_off => trm_off
  | trm_append t1 t2 => trm_append ([z ~> u]t1) ([z ~> u]t2)
  | trm_fvar x => If x = z then u else trm_fvar x
  | trm_app t1 t2 => trm_app ([z ~> u]t1) ([z ~> u]t2)
  | trm_abs t1 => trm_abs ([z ~> u]t1)
  | trm_sapp t1 t2 => trm_sapp ([z ~> u]t1) ([z ~> u]t2)
  | trm_sabs t1 => trm_sabs ([z ~> u]t1)
  | trm_fix t1 => trm_fix ([z ~> u]t1)
  | trm_choice t1 t2 t3 => trm_choice ([z ~> u]t1) ([z ~> u]t2) ([z ~> u]t3)
  end
where "[ z ~> u ] t" := (subst_trm z u t)
.

Reserved Notation "[ ' z ~> u ] t" (at level 0, t at level 0).
Fixpoint nsubst_trm (z : var) (u : trm) (t : trm) {struct t} : trm :=
  match t with
  | trm_bvar i => trm_bvar i
  | trm_empty => trm_empty
  | trm_single t1 => trm_single (['z ~> u]t1)
  | trm_dot t1 t2 => trm_dot (['z ~> u]t1) (['z ~> u]t2)
  | trm_plus t1 t2 => trm_plus (['z ~> u]t1) (['z ~> u]t2)
  | trm_kstar t1 => trm_kstar (['z ~> u]t1)
  | trm_star => trm_star
  | trm_nat => trm_nat
  | trm_arrow t1 t2 t3 => trm_arrow (['z ~> u]t1) (['z ~> u]t2) (['z ~> u]t3)
  | trm_forall t1 t2 => trm_forall (['z ~> u]t1) (['z ~> u]t2)
  | trm_nvar x => If x = z then u else trm_nvar x
  | trm_eps => trm_eps
  | trm_on => trm_on
  | trm_off => trm_off
  | trm_append t1 t2 => trm_append (['z ~> u]t1) (['z ~> u]t2)
  | trm_fvar x => trm_fvar x
  | trm_app t1 t2 => trm_app (['z ~> u]t1) (['z ~> u]t2)
  | trm_abs t1 => trm_abs (['z ~> u]t1)
  | trm_sapp t1 t2 => trm_sapp (['z ~> u]t1) (['z ~> u]t2)
  | trm_sabs t1 => trm_sabs (['z ~> u]t1)
  | trm_fix t1 => trm_fix (['z ~> u]t1)
  | trm_choice t1 t2 t3 => trm_choice (['z ~> u]t1) (['z ~> u]t2) (['z ~> u]t3)
  end
where "[ ' z ~> u ] t" := (nsubst_trm z u t)
.

(** ** Locally closed terms  *)

Inductive name : trm -> Prop :=
| name_var : forall a,
    name (trm_nvar a)
| name_eps :
    name trm_eps
| name_on :
    name trm_on
| name_off :
    name trm_off
| name_append : forall n1 n2,
    name n1 ->
    name n2 ->
    name (trm_append n1 n2)
.

Inductive effect : trm -> Prop :=
| effect_empty :
    effect trm_empty
| effect_single : forall n,
    name n ->
    effect (trm_single n)
| effect_dot : forall E1 E2,
    effect E1 ->
    effect E2 ->
    effect (trm_dot E1 E2)
| effect_plus : forall E1 E2,
    effect E1 ->
    effect E2 ->
    effect (trm_plus E1 E2)
| effect_star : forall E,
    effect E ->
    effect (trm_kstar E)
.

Inductive typ : trm -> Prop :=
| typ_nat :
    typ trm_nat
| typ_arrow : forall E T1 T2,
    effect E ->
    typ T1 ->
    typ T2 ->
    typ (trm_arrow T1 E T2)
| typ_forall : forall L E T,
    (forall a, a \notin L -> effect (E ^ 'a)) ->
    (forall a, a \notin L -> typ (T ^ 'a)) ->
    typ (trm_forall E T)
.

Inductive expr : trm -> Prop :=
| expr_var : forall x,
    expr (trm_fvar x)
| expr_app : forall e1 e2,
    expr e1 ->
    expr e2 ->
    expr (trm_app e1 e2)
| expr_abs : forall L e,
    (forall x, x \notin L -> expr (e ^ x)) ->
    expr (trm_abs e)
| expr_sapp : forall e n,
    expr e ->
    name n ->
    expr (trm_sapp e n)
| expr_sabs : forall L e,
    (forall a, a \notin L -> expr (e ^ 'a)) ->
    expr (trm_sabs e)
| expr_fix : forall L e,
    (forall f, f \notin L -> expr (e ^ f)) ->
    expr (trm_fix e)
| expr_choice : forall e1 e2 n,
    expr e1 ->
    expr e2 ->
    name n ->
    expr (trm_choice e1 n e2)
.

Inductive value : trm -> Prop :=
| value_abs : forall e,
    expr (trm_abs e) ->
    value (trm_abs e)
| value_sabs : forall e,
    expr (trm_sabs e) ->
    value (trm_sabs e)
.
Print Grammar constr.
Inductive environment : env -> Prop :=
| environment_empty :
    environment empty
| environment_push : forall G x T,
    environment G ->
    x # G ->
    typ T ->
    environment (G & x ~ T)
| environment_npush : forall G a,
    environment G ->
    a # G ->
    environment (G & 'a)
.

Inductive orthant : orth -> Prop :=
| orthant_intro : forall D,
    (forall n, (inl n) ∈ D -> name n) ->
    (forall n, (inr n) ∈ D -> name n) ->
    orthant D
.

(** ** Semantics *)

Reserved Notation "e1 --> D e2" (at level 68, D at level 0).
Inductive step : trm -> orth -> trm -> Prop :=
| step_beta : forall D e v,
    orthant D ->
    expr (trm_abs e) ->
    value v ->
    (trm_app (trm_abs e) v) -->D e ^^ v
| step_sigma : forall D e n,
    orthant D ->
    expr (trm_sabs e) ->
    name n ->
    (trm_sapp (trm_sabs e) n) -->D e ^^ n
| step_fix : forall D e,
    orthant D ->
    expr (trm_fix (trm_abs e)) ->
    (trm_fix (trm_abs e)) -->D (trm_abs e) ^^ (trm_fix (trm_abs e))
| step_dist_app_l : forall D e1 e2 e3 n,
    orthant D ->
    expr e1 ->
    expr e2 ->
    expr e3 ->
    name n ->
    (trm_app (trm_choice e1 n e2) e3)
      -->D (trm_choice (trm_app e1 e3) n (trm_app e2 e3))
| step_dist_app_r : forall D e1 e2 v n,
    orthant D ->
    expr e1 ->
    expr e2 ->
    value v ->
    name n ->
    (trm_app v (trm_choice e1 n e2))
      -->D (trm_choice (trm_app v e1) n (trm_app v e2))
| step_dist_sapp : forall D e1 e2 n1 n2,
    orthant D ->
    expr e1 ->
    expr e2 ->
    name n1 ->
    name n2 ->
    (trm_sapp (trm_choice e1 n1 e2) n2)
      -->D (trm_choice (trm_sapp e1 n2) n1 (trm_sapp e2 n2))
| step_app_l : forall D e1 e1' e2,
    orthant D ->
    expr e1 ->
    expr e2 ->
    e1 -->D e1' ->
    (trm_app e1 e2) -->D (trm_app e1' e2)
| step_app_r : forall D e e' v,
    orthant D ->
    expr e ->
    value v ->
    e -->D e' ->
    (trm_app v e) -->D (trm_app v e')
| step_sapp : forall D e e' n,
    orthant D ->
    expr e ->
    name n ->
    e -->D e' ->
    (trm_sapp e n) -->D (trm_sapp e' n)
| step_choice_l : forall D e1 e1' e2 n,
    orthant D ->
    expr e1 ->
    expr e2 ->
    name n ->
    e1 -->(D ⊌ (inl n)) e1' ->
    (trm_choice e1 n e2) -->D (trm_choice e1' n e2)
| step_choice_r : forall D e1 e2 e2' n,
    orthant D ->
    expr e1 ->
    expr e2 ->
    name n ->
    e2 -->(D ⊌ (inr n)) e2' ->
    (trm_choice e1 n e2) -->D (trm_choice e1 n e2')
| step_world_l : forall D e1 e2 n,
    orthant D ->
    expr e1 ->
    expr e2 ->
    (inl n) ∈ D ->
    (trm_choice e1 n e2) -->D e1
| step_world_r : forall D e1 e2 n,
    orthant D ->
    expr e1 ->
    expr e2 ->
    (inr n) ∈ D ->
    (trm_choice e1 n e2) -->D e2
where "e1 --> D e2" := (step e1 D e2)
.

(** ** Type system *)

Inductive wf_env : env -> Prop :=
| wf_env_empty :
    wf_env empty
| wf_env_push : forall G x T,
    wf_env G ->
    wf_typ G T ->
    x # G ->
    wf_env (G & x ~ T)
| wf_env_spush : forall G a,
    wf_env G ->
    a # G ->
    wf_env (G & 'a)
with wf_name : env -> trm -> Prop :=
| wf_name_var : forall G a,
    wf_env G ->
    binds a trm_star G ->
    wf_name G (trm_nvar a)
| wf_name_eps : forall G,
    wf_env G ->
    wf_name G trm_eps
| wf_name_on : forall G,
    wf_env G ->
    wf_name G trm_on
| wf_name_off : forall G,
    wf_env G ->
    wf_name G trm_off
| wf_name_append : forall G n1 n2,
    wf_name G n1 ->
    wf_name G n2 ->
    wf_name G (trm_append n1 n2)
with wf_effect : env -> trm -> Prop :=
| wf_effect_empty : forall G,
    wf_env G ->
    wf_effect G trm_empty
| wf_effect_single : forall G n,
    wf_name G n ->
    wf_effect G (trm_single n)
| wf_effect_dot : forall G E1 E2,
    wf_effect G E1 ->
    wf_effect G E2 ->
    wf_effect G (trm_dot E1 E2)
| wf_effect_plus : forall G E1 E2,
    wf_effect G E1 ->
    wf_effect G E2 ->
    wf_effect G (trm_plus E1 E2)
| wf_effect_star : forall G E,
    wf_effect G E ->
    wf_effect G (trm_kstar E)
with wf_typ : env -> trm -> Prop :=
| wf_typ_nat : forall G,
    wf_env G ->
    wf_typ G trm_nat
| wf_typ_arrow : forall G E T1 T2,
    wf_effect G E ->
    wf_typ G T1 ->
    wf_typ G T2 ->
    wf_typ G (trm_arrow T1 E T2)
| wf_typ_forall : forall L G E T,
    (forall a, a \notin L -> wf_effect (G & 'a) (E ^ 'a)) ->
    (forall a, a \notin L -> wf_typ (G & 'a) (T ^ 'a)) ->
    wf_typ G (trm_forall E T)
.

Parameter eff_disjoint_core : trm -> trm -> Prop.

Definition eff_disjoint (E1 : trm) (E2 : trm) : Prop :=
  effect E1 /\
  effect E2 /\
  eff_disjoint_core E1 E2.

Parameter eff_sub_core : trm -> trm -> Prop.

Definition eff_sub (E1 : trm) (E2 : trm) : Prop :=
  effect E1 /\
  effect E2 /\
  eff_sub_core E1 E2.

Inductive typ_sub : trm -> trm -> Prop :=
| typ_sub_nat :
    typ_sub trm_nat trm_nat
| typ_sub_arrow : forall T1 T2 T3 T4 E1 E2,
    typ_sub T3 T1 ->
    typ_sub T2 T4 ->
    eff_sub E1 E2 ->
    typ_sub (trm_arrow T1 E1 T2) (trm_arrow T3 E2 T4)
| typ_sub_forall : forall L T1 T2 E1 E2,
    (forall a, a \notin L -> typ_sub (T1 ^ 'a) (T2 ^ 'a)) ->
    (forall a, a \notin L -> eff_sub (E1 ^ 'a) (E2 ^ 'a)) ->
    typ_sub (trm_forall E1 T1) (trm_forall E2 T2)
.

Reserved Notation "G |= e ~: T & E" (at level 69, T at level 27).
Inductive typing : env -> trm -> trm -> trm -> Prop :=
| typing_var : forall G x T,
    typ T ->
    wf_env G ->
    binds x T G ->
    G |= (trm_fvar x) ~: T & trm_empty
| typing_app : forall G e1 e2 T1 T2 n E1 E2 E3,
    G |= e1 ~: (trm_arrow T1 (trm_dot (trm_single n) E1) T2) & (trm_dot (trm_single n) E2) ->
    G |= e2 ~: T1 & (trm_dot (trm_single n) E3) ->
    eff_disjoint E1 E2 ->
    eff_disjoint E2 E3 ->
    eff_disjoint E3 E1 ->
    (forall a, a \notin fnv_trm E1) ->
    (forall a, a \notin fnv_trm E2) ->
    (forall a, a \notin fnv_trm E3) ->
    G |= (trm_app e1 e2) ~: T2 & (trm_dot (trm_single n) (trm_plus E1 (trm_plus E2 E3)))
| typing_abs : forall L G e T1 T2 E,
    typ T1 ->
    (forall x, x \notin L -> (G & x ~ T1) |= (e ^ x) ~: T2 & E) ->
    G |= (trm_abs e) ~: (trm_arrow T1 E T2) & trm_empty
| typing_sapp : forall G e n T n' E1 E2 E3,
    G |= e ~: (trm_forall E1 T) & (trm_dot (trm_single n') E2) ->
    wf_name G n ->
    wf_effect G (trm_dot (trm_single n') E3) ->
    eff_sub (E1 ^^ n) (trm_dot (trm_single n') E3) ->
    eff_disjoint E2 E3 ->
    (forall a, a \notin fnv_trm E2) ->
    (forall a, a \notin fnv_trm E3) ->
    G |= (trm_sapp e n) ~: (T ^^ n) & (trm_dot (trm_single n') (trm_plus E2 E3))
| typing_sabs : forall L G e T E,
    (forall a, a \notin L -> (G & 'a) |= (e ^ 'a) ~: (T ^ 'a) & (E ^ 'a)) ->
    G |= (trm_sabs e) ~: (trm_forall E T) & trm_empty
| typing_fix : forall L G e T1 T2 E,
    (forall f, f \notin L -> (G & f ~ (trm_arrow T1 E T2)) |= ((trm_abs e) ^ f) ~: (trm_arrow T1 E T2) & trm_empty) ->
    G |= (trm_fix (trm_abs e)) ~: (trm_arrow T1 E T2) & trm_empty
| typing_choice : forall G e1 e2 n T n' E1 E2,
    G |= e1 ~: T & (trm_dot (trm_single n') E1) ->
    G |= e2 ~: T & (trm_dot (trm_single n') E1) ->
    wf_name G n ->
    wf_effect G (trm_dot (trm_single n') E2) ->
    eff_sub (trm_single n) (trm_dot (trm_single n') E2) ->
    eff_disjoint E1 E2 ->
    (forall a, a \notin fnv_trm E1) ->
    (forall a, a \notin fnv_trm E2) ->
    G |= (trm_choice e1 n e2) ~: T & (trm_dot (trm_single n') (trm_plus E1 E2))
| typing_sub : forall G e T1 T2 E1 E2,
    G |= e ~: T1 & E1 ->
    wf_typ G T2 ->
    wf_effect G E2 ->
    typ_sub T1 T2 ->
    eff_sub E1 E2 ->
    G |= e ~: T2 & E2
where "G |= e ~: T & E" := (typing G e T E)
.

Reserved Notation "e1 ==> e2" (at level 68).
Inductive pstep : trm -> trm -> Prop :=
| pstep_beta : forall e v,
    expr (trm_abs e) ->
    value v ->
    (trm_app (trm_abs e) v) ==> e ^^ v
| pstep_sigma : forall e n,
    expr (trm_sabs e) ->
    name n ->
    (trm_sapp (trm_sabs e) n) ==> e ^^ n
| pstep_fix : forall e,
    expr (trm_fix (trm_abs e)) ->
    (trm_fix (trm_abs e)) ==> (trm_abs e) ^^ (trm_fix (trm_abs e))
| pstep_dist_app_l : forall e1 e2 e3 n,
    expr e1 ->
    expr e2 ->
    expr e3 ->
    name n ->
    (trm_app (trm_choice e1 n e2) e3)
      ==> (trm_choice (trm_app e1 e3) n (trm_app e2 e3))
| pstep_dist_app_r : forall e1 e2 v n,
    expr e1 ->
    expr e2 ->
    value v ->
    name n ->
    (trm_app v (trm_choice e1 n e2))
      ==> (trm_choice (trm_app v e1) n (trm_app v e2))
| pstep_dist_sapp : forall e1 e2 n1 n2,
    expr e1 ->
    expr e2 ->
    name n1 ->
    name n2 ->
    (trm_sapp (trm_choice e1 n1 e2) n2)
      ==> (trm_choice (trm_sapp e1 n2) n1 (trm_sapp e2 n2))
| pstep_app_l : forall e1 e1' e2,
    expr e1 ->
    expr e2 ->
    e1 ==> e1' ->
    (trm_app e1 e2) ==> (trm_app e1' e2)
| pstep_app_r : forall e e' v,
    expr e ->
    value v ->
    e ==> e' ->
    (trm_app v e) ==> (trm_app v e')
| pstep_sapp : forall e e' n,
    expr e ->
    name n ->
    e ==> e' ->
    (trm_sapp e n) ==> (trm_sapp e' n)
| pstep_choice_l : forall e1 e1' e2 n,
    expr e1 ->
    expr e2 ->
    name n ->
    e1 ==> e1' ->
    (trm_choice e1 n e2) ==> (trm_choice e1' n e2)
| pstep_choice_r : forall e1 e2 e2' n,
    expr e1 ->
    expr e2 ->
    name n ->
    e2 ==> e2' ->
    (trm_choice e1 n e2) ==> (trm_choice e1 n e2')
where "e1 ==> e2" := (pstep e1 e2)
.
