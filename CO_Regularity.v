Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From CO Require Export CO_Infrastructure.

Section Slang.

Local Open Scope s_scope.

Lemma subst_sexpr : forall x u e,
    sexpr u ->
    sexpr e ->
    sexpr [x ~> u]e.
Proof.
  induction 2; calc_open_subst; auto.
  - apply_fresh sexpr_abs. rewrite~ <- subst_open_var_strm.
  - apply_fresh sexpr_fix. rewrite~ <- subst_open_var_strm.
Qed.

Local Hint Resolve subst_sexpr.

Lemma open_abs_body_sexpr : forall e u,
    sexpr (strm_abs e) ->
    sexpr u ->
    sexpr (e ^^ u).
Proof. intros. inverts H. pick_fresh y. rewrite~ (@subst_intro_strm y). Qed.

Lemma open_fix_body_sexpr : forall e u,
    sexpr (strm_fix e) ->
    sexpr u ->
    sexpr (e ^^ u).
Proof. intros. inverts H. pick_fresh y. rewrite~ (@subst_intro_strm y). Qed.

Lemma styping_regular_G : forall G e T,
    G |= e ~: T ->
    ok G.
Proof.
  induction 1; auto.
  - specialize_fresh. auto.
  - specialize_fresh. auto.
Qed.

Lemma styping_regular_e : forall G e T,
    G |= e ~: T ->
    sexpr e.
Proof.
  induction 1; auto.
  - apply_fresh* sexpr_abs.
  - apply_fresh* sexpr_fix.
Qed.

End Slang.

Hint Resolve subst_sexpr.

Hint Extern 1 (ok ?G) =>
match goal with
| [ H: (G |= _ ~: _)%s |- _ ] => apply (styping_regular_G H)
end.

Hint Extern 1 (sexpr ?e) =>
match goal with
| [ H: (_ |= e ~: _)%s |- _ ] => apply (styping_regular_e H)
end.

(** ** Automation from sub terms *)

Lemma name_append_inv : forall n1 n2,
    name (trm_append n1 n2) ->
    name n1 /\ name n2.
Proof. by inversion 1. Qed.

Hint Extern 1 (name ?n) =>
match goal with
| [ H: context [trm_append n ?n'] |- _ ] => apply (@name_append_inv n n')
| [ H: context [trm_append ?n' n] |- _ ] => apply (@name_append_inv n' n)
end.

Lemma effect_single_inv : forall n,
    effect (trm_single n) ->
    name n.
Proof. inversion 1; auto. Qed.

Lemma effect_dot_inv : forall E1 E2,
    effect (trm_dot E1 E2) ->
    effect E1 /\ effect E2.
Proof. inversion 1; auto. Qed.

Lemma effect_plus_inv : forall E1 E2,
    effect (trm_plus E1 E2) ->
    effect E1 /\ effect E2.
Proof. inversion 1; auto. Qed.

Lemma effect_star_inv : forall E,
    effect (trm_kstar E) ->
    effect E.
Proof. inversion 1; auto. Qed.

Hint Extern 1 (name ?n) =>
match goal with
| [ H: context [trm_single ?n] |- _ ] => apply (@effect_single_inv n)
end.

Hint Extern 1 (effect ?E) =>
match goal with
| [ H: context [trm_dot E ?E'] |- _ ] => apply (@effect_dot_inv E E')
| [ H: context [trm_dot ?E' E] |- _ ] => apply (@effect_dot_inv E' E)
| [ H: context [trm_plus E ?E'] |- _ ] => apply (@effect_plus_inv E E')
| [ H: context [trm_plus ?E' E] |- _ ] => apply (@effect_plus_inv E' E)
| [ H: context [trm_kstar E] |- _ ] => apply (@effect_star_inv E)
end.

Lemma typ_arrow_inv : forall T1 T2 E,
    typ (trm_arrow T1 E T2) ->
    typ T1 /\ typ T2 /\ effect E.
Proof. inversion 1; auto. Qed.

Hint Extern 1 (typ ?T) =>
match goal with
| [ H: context [trm_arrow T ?E ?T'] |- _ ] => apply (@typ_arrow_inv T T' E)
| [ H: context [trm_arrow ?T' ?E T] |- _ ] => apply (@typ_arrow_inv T' T E)
end.

Hint Extern 1 (effect ?E) =>
match goal with
| [ H: context [trm_arrow ?T E ?T'] |- _ ] => apply (@typ_arrow_inv T T' E)
end.

(** ** Sub categories of syntax *)

Lemma environment_is_ok : forall G,
    environment G ->
    ok G.
Proof. induction 1; auto. Qed.

Lemma value_is_expr : forall v,
    value v ->
    expr v.
Proof. induction 1; auto. Qed.

Hint Resolve environment_is_ok value_is_expr.

(** ** Well-definedness of substitutions *)

Lemma subst_name : forall x u n,
    expr u ->
    name n ->
    name [x ~> u]n.
Proof. induction 2; calc_open_subst; auto. Qed.

Lemma subst_effect : forall x u E,
    expr u ->
    effect E ->
    effect [x ~> u]E.
Proof. induction 2; calc_open_subst; auto using subst_name. Qed.

Lemma subst_typ : forall x u T,
    expr u ->
    typ T ->
    typ [x ~> u]T.
Proof.
  induction 2; calc_open_subst; auto using subst_effect.
  - apply_fresh typ_forall.
    + rewrite~ <- subst_open_nvar_trm. auto using subst_effect.
    + rewrite~ <- subst_open_nvar_trm.
Qed.

Lemma subst_expr : forall x u e,
    expr u ->
    expr e ->
    expr ([x ~> u]e).
Proof.
  induction 2; calc_open_subst; auto using subst_name.
  - apply_fresh expr_abs. rewrite~ <- subst_open_var_trm.
  - apply_fresh expr_sabs. rewrite~ <- subst_open_nvar_trm.
  - apply_fresh expr_fix. rewrite~ <- subst_open_var_trm.
Qed.

Hint Resolve subst_name subst_effect subst_typ subst_expr.

Lemma nsubst_name  : forall a u b,
    name u ->
    name b ->
    name ['a ~> u]b.
Proof. induction 2; calc_open_subst; auto. Qed.

Lemma nsubst_effect : forall a u E,
    name u ->
    effect E ->
    effect ['a ~> u]E.
Proof. induction 2; calc_open_subst; auto using nsubst_name. Qed.

Lemma nsubst_typ : forall a u T,
    name u ->
    typ T ->
    typ ['a ~> u]T.
Proof.
  induction 2; calc_open_subst; auto using nsubst_effect.
  - apply_fresh typ_forall.
    + rewrite~ <- nsubst_open_nvar_trm. auto using nsubst_effect.
    + rewrite~ <- nsubst_open_nvar_trm.
Qed.

Lemma nsubst_expr : forall a u e,
    name u ->
    expr e ->
    expr ['a ~> u]e.
Proof.
  induction 2; calc_open_subst; auto using nsubst_name.
  - apply_fresh expr_abs. rewrite~ <- nsubst_open_var_trm.
  - apply_fresh expr_sabs. rewrite~ <- nsubst_open_nvar_trm.
  - apply_fresh expr_fix. rewrite~ <- nsubst_open_var_trm.
Qed.

Hint Resolve nsubst_name nsubst_effect nsubst_typ nsubst_expr.

(** *** correct intention of bindings *)

Lemma open_abs_body_expr : forall e u,
    expr (trm_abs e) ->
    expr u ->
    expr (e ^^ u).
Proof. intros. inverts H. pick_fresh y. rewrite~ (@subst_intro_trm y). Qed.

Lemma open_sabs_body_expr : forall e u,
    expr (trm_sabs e) ->
    name u ->
    expr (e ^^ u).
Proof. intros. inverts H. pick_fresh a. rewrite~ (@nsubst_intro_trm a). Qed.

Lemma open_fix_body_expr : forall e u,
    expr (trm_fix e) ->
    expr u ->
    expr (e ^^ u).
Proof. intros. inverts H. pick_fresh y. rewrite~ (@subst_intro_trm y). Qed.

Lemma nopen_forall_body_effect : forall E T n,
    typ (trm_forall E T) ->
    name n ->
    effect (E ^^ n).
Proof. intros. inverts H. pick_fresh a. rewrite~ (@nsubst_intro_trm a). Qed.

Lemma nopen_forall_body_typ : forall E T n,
    typ (trm_forall E T) ->
    name n ->
    typ (T ^^ n).
Proof. intros. inverts H. pick_fresh a. rewrite~ (@nsubst_intro_trm a). Qed.

(** ** Well-defindeness of semantics *)

Lemma step_regular_pre : forall e1 e2 D,
    e1 -->D e2 ->
    expr e1.
Proof.
  induction 1; auto.
  - destruct H. auto.
  - destruct H. auto.
Qed.

Lemma step_regular_post : forall e1 e2 D,
    e1 -->D e2 ->
    expr e2.
Proof.
  induction 1; auto.
  - auto using open_abs_body_expr.
  - auto using open_sabs_body_expr.
  - auto using open_fix_body_expr.
Qed.

Lemma step_regular_orthant : forall e1 e2 D,
    e1 -->D e2 ->
    orthant D.
Proof. induction 1; auto. Qed.

Hint Extern 1 (expr ?e) =>
match goal with
| [ H: e -->_ _ |- _ ] => exact (step_regular_pre H)
| [ H: _ -->_ e |- _ ] => exact (step_regular_post H)
end.

Hint Extern 1 (orthant ?D) =>
match goal with
| [ H: _ -->D _ |- _ ] => exact (step_regular_orthant H)
end.

(** ** Well-definedness of type system *)

Lemma environment_push_inv : forall G x u,
    environment (G & x ~ u) ->
    environment G /\ (typ u \/ u = trm_star).
Proof.
  intros* H. inverts H.
  - false* empty_push_inv.
  - apply eq_push_inv in H0. unpack; subst. auto.
  - apply eq_push_inv in H0. unpack; subst. auto.
Qed.

Lemma environment_push_inv_G : forall G x u,
    environment (G & x ~ u) ->
    environment G.
Proof. intros. eapply environment_push_inv. eassumption. Qed.

Lemma wf_env_push_inv : forall G x u,
    wf_env (G & x ~ u) ->
    wf_env G /\ (wf_typ G u \/ u = trm_star).
Proof.
  inversion 1; subst.
  - false* empty_push_inv.
  - apply eq_push_inv in H0. unpack. subst. auto.
  - apply eq_push_inv in H0. unpack. subst. auto.
Qed.

Lemma wf_env_wf_name_wf_effect_wf_typ_regular :
  (forall G, wf_env G -> environment G) /\
  (forall G n, wf_name G n -> environment G /\ name n) /\
  (forall G E, wf_effect G E -> environment G /\ effect E) /\
  (forall G T, wf_typ G T -> environment G /\ typ T).
Proof.
  apply wf_env_wf_name_wf_effect_wf_typ_ind; intros; try split; autos*.
  - specialize_fresh. autos* environment_push_inv_G.
  - apply_fresh typ_forall.
    + apply* H0.
    + apply* H2.
Qed.

Lemma wf_env_regular : forall G,
    wf_env G ->
    environment G.
Proof. auto using (proj41 wf_env_wf_name_wf_effect_wf_typ_regular). Qed.

Lemma wf_name_regular_G : forall G n,
    wf_name G n ->
    environment G.
Proof.
  intros. by apply ((proj42 wf_env_wf_name_wf_effect_wf_typ_regular) G n).
Qed.

Lemma wf_name_regular_n : forall G n,
    wf_name G n ->
    name n.
Proof.
  intros. by apply ((proj42 wf_env_wf_name_wf_effect_wf_typ_regular) G n).
Qed.

Lemma wf_effect_regular_G : forall G E,
    wf_effect G E ->
    environment G.
Proof.
  intros. by apply ((proj43 wf_env_wf_name_wf_effect_wf_typ_regular) G E).
Qed.

Lemma wf_effect_regular_E : forall G E,
    wf_effect G E ->
    effect E.
Proof.
  intros. by apply ((proj43 wf_env_wf_name_wf_effect_wf_typ_regular) G E).
Qed.

Lemma wf_typ_regular_G : forall G T,
    wf_typ G T ->
    environment G.
Proof.
  intros. by apply ((proj44 wf_env_wf_name_wf_effect_wf_typ_regular) G T).
Qed.

Lemma wf_typ_regular_T : forall G T,
    wf_typ G T ->
    typ T.
Proof.
  intros. by apply ((proj44 wf_env_wf_name_wf_effect_wf_typ_regular) G T).
Qed.

Hint Resolve wf_env_regular.

Hint Extern 1 (environment ?G) =>
match goal with
| [ H: wf_name G _ |- _ ] => exact (wf_name_regular_G H)
| [ H: wf_effect G _ |- _ ] => exact (wf_effect_regular_G H)
| [ H: wf_typ G _ |- _ ] => exact (wf_typ_regular_G H)
end.

Hint Extern 1 (name ?n) =>
match goal with
| [ H: wf_name _ n |- _ ] => exact (wf_name_regular_n H)
end.

Hint Extern 1 (effect ?E) =>
match goal with
| [ H: wf_effect _ E |- _ ] => exact (wf_effect_regular_E H)
end.

Hint Extern 1 (typ ?T) =>
match goal with
| [ H: wf_typ _ T |- _ ] => exact (wf_typ_regular_T H)
end.

Lemma eff_disjoint_regular : forall E1 E2,
    eff_disjoint E1 E2 ->
    effect E1 /\ effect E2.
Proof. unfold eff_disjoint. intuition. Qed.

Hint Extern 1 (effect ?E) =>
match goal with
| [ H: eff_disjoint E _ |- _ ] => exact (proj21 (eff_disjoint_regular H))
| [ H: eff_disjoint _ E |- _ ] => exact (proj22 (eff_disjoint_regular H))
end.

Lemma eff_sub_regular : forall E1 E2,
    eff_sub E1 E2 ->
    effect E1 /\ effect E2.
Proof. unfold eff_sub. intuition. Qed.

Hint Extern 1 (effect ?E) =>
match goal with
| [ H: eff_sub E _ |- _ ] => exact (proj21 (eff_sub_regular H))
| [ H: eff_sub _ E |- _ ] => exact (proj22 (eff_sub_regular H))
end.

Lemma eff_sub_regular_L : forall E1 E2,
    eff_sub E1 E2 ->
    effect E1.
Proof. auto. Qed.

Lemma eff_sub_regular_R : forall E1 E2,
    eff_sub E1 E2 ->
    effect E2.
Proof. auto. Qed.

Lemma typ_sub_regular : forall T1 T2,
    typ_sub T1 T2 ->
    typ T1 /\ typ T2.
Proof.
  induction 1; intuition.
  - apply_fresh typ_forall.
    + apply* eff_sub_regular_L.
    + apply* H0.
  - apply_fresh typ_forall.
    + apply* eff_sub_regular_R.
    + apply* H0.
Qed.

Hint Extern 1 (typ ?T) =>
match goal with
| [ H: typ_sub T _ |- _ ] => exact (proj21 (typ_sub_regular H))
| [ H: typ_sub _ T |- _ ] => exact (proj22 (typ_sub_regular H))
end.

Lemma typ_sub_regular_L : forall T1 T2,
    typ_sub T1 T2 ->
    typ T1.
Proof. auto. Qed.

Lemma typ_sub_regular_R : forall T1 T2,
    typ_sub T1 T2 ->
    typ T2.
Proof. auto. Qed.

Lemma typing_regular_G : forall G e T E,
    G |= e ~: T & E ->
    environment G.
Proof.
  induction 1; auto; specialize_fresh; eauto using environment_push_inv_G.
Qed.

Lemma typing_regular_e : forall G e T E,
    G |= e ~: T & E ->
    expr e.
Proof.
  induction 1; auto.
  - apply_fresh~ expr_abs.
  - apply_fresh~ expr_sabs.
  - apply_fresh~ expr_fix.
Qed.

Lemma typing_regular_TE : forall G e T E,
    G |= e ~: T & E ->
    typ T /\ effect E.
Proof.
  induction 1; split; unpack; auto.
  - specialize_fresh. unpack. auto.
  - eauto using nopen_forall_body_typ.
  - apply_fresh typ_forall.
    + apply* H0.
    + apply* H0.
  - specialize_fresh. unpack. auto.
Qed.

Lemma typing_regular_T : forall G e T E,
    G |= e ~: T & E ->
    typ T.
Proof. intros. apply (typing_regular_TE H). Qed.

Lemma typing_regular_E : forall G e T E,
    G |= e ~: T & E ->
    effect E.
Proof. intros. apply (typing_regular_TE H). Qed.

Hint Extern 1 (environment ?G) =>
match goal with
| [ H: G |= _ ~: _ & _ |- _ ] => exact (typing_regular_G H)
end.

Hint Extern 1 (expr ?e) =>
match goal with
| [ H: _ |= e ~: _ & _ |- _ ] => exact (typing_regular_e H)
end.

Hint Extern 1 (typ ?T) =>
match goal with
| [ H: _ |= _ ~: T & _ |- _ ] => exact (typing_regular_T H)
end.

Hint Extern 1 (effect ?E) =>
match goal with
| [ H: _ |= _ ~: _ & E |- _ ] => exact (typing_regular_E H)
end.
